package TTclases;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;

public class JSON2XML 
{

    public JSON2XML() {
    }
    
    public String convert(String json, String root)
    {
        String xml="";
        try {
            org.json.JSONArray jsonFileObject = new org.json.JSONArray(json);
            xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-15\"?>\n<"+root+">"
                    + org.json.XML.toString(jsonFileObject,"row") + "</"+root+">";
            
        }catch (JSONException ex) {
            System.out.println(ex.toString());
        }
        return xml;
    }
    
    public String readFile(String filepath)
    {
        StringBuilder sb = new StringBuilder();
        try{
        InputStream in = new FileInputStream(filepath);
        Charset encoding = Charset.forName("UTF-8");
        Reader reader = new InputStreamReader(in, encoding);
        
        int r = 0,x=0;
        while ((r = reader.read()) != -1)
        {
            char ch = (char) r;
            if(ch!='[' && x==0){
            }else if(ch == '\n'){
                sb.append(" ");
            }else{
                sb.append(ch);
            }
            if(ch=='}'){
                sb.append("\n");
            }
            x++;
        }
        
        in.close();
        reader.close();
        }catch (IOException ex) {
            System.out.println(ex.toString());
        }
        
        return sb.toString();
    }
    
    public void writeFile(String filepath, String output)
    {
        FileWriter ofstream = null;
        try {
            ofstream = new FileWriter(filepath);
            try (BufferedWriter out = new BufferedWriter(ofstream)) {
                out.write(output);
            }
            catch (IOException ex) {
                System.out.println(ex.toString());
            }
        }
        catch (IOException ex) {
            Logger.getLogger(JSON2XML.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ofstream.close();
            } catch (IOException ex) {
                Logger.getLogger(JSON2XML.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}