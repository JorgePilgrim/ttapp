
package TTclases;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.jespxml.JespXML;
import org.jespxml.modelo.Tag;

/**
 *
 * @author ALAN
 */
public class Archivo {
    
    private String sitio;
    private String nombre;
    private String ruta;
    private List<List> listacor;
    
    
    //COLUMNAS/ATRIBUTOS
    private List<String> tiposCol;
    private List<String> errxat;
    private long totalerr;
    private int numCol; //LISTO
    private List<Atributo> columnas;
    
    //FILAS/REIGISTROS
    private long tamFilas;//LISTO
    List<Tag> filas;//LISTO
    
    //OTROS
    private Tag raiz;
    String valorHijo;
    List<Tag> etqHijos;
    
    
    public Archivo(String ruta,String sitio,String nombre,List<List> listacor,List<String> tiposCol){
        this.ruta= ruta;
        this.sitio=sitio;
        this.nombre= nombre;
        this.tiposCol= tiposCol;
        this.listacor=listacor;
        columnas=new ArrayList<>();
    }

    public int getNumCol() {
        return numCol;
    }

    public String getSitio() {
        return sitio;
    }

    public String getNombre() {
        return nombre;
    }

    public long getTamFilas() {
        return tamFilas;
    }

    public String getRuta() {
        return ruta;
    }
    
    public List<Atributo> getColumnas() {
        return columnas;
    }
    
    public int gettotalerr(){
        int total=0;
        if(errxat.size()>0){
            for (int i = 0; i < errxat.size(); i++) {
                total+=Integer.parseInt(errxat.get(i));
            }
            return total;
        }else{
            return 0;
        }
    }
    
     public void setErrxat(List<String> errxat) {
        this.errxat = errxat;
    }
    
    

    public void iniciar(){
        try {
            JespXML xmlDoc = new JespXML(new File(ruta.trim()));
            raiz = xmlDoc.leerXML();
            filas=new ArrayList(raiz.getTagsHijos());
            tamFilas=filas.size();
            numCol=filas.get(3).getTagsHijos().size();
            totalerr=gettotalerr();
        } catch (Exception e) {
            System.out.println("Error al iniciar");
        }
    }
    
    public void preparar(){
        int listafilastam =listacor.get(1).size();
        
        for (int j = 0; j < numCol; j++) {
            System.out.println("Preparando columna "+j);
            //VAR PROM
            double promD =0;
            int serror=0;
            long comp;
            boolean bool;

            //VAR MODA
            int maximaVecesQueSeRepite = 0;
            String moda="";

            //VAR MAX
            double valMax=0;

            //VAR MIN
            double valMin=0;
            
            double aux;
            
            Atributo c = new Atributo();
            c.setNum(j);
            c.setNombre(filas.get(j).getTagsHijos().get(j).getNombre());
            c.setTipo(tiposCol.get(j));
            c.setErr(errxat.get(j));
            
            for (int i = 0; i < tamFilas; i++) {
                etqHijos=new ArrayList(filas.get(i).getTagsHijos());
                valorHijo = etqHijos.get(c.getNum()).getContenido();
                
                if(c.getTipo().contains("numero")){
                    //PROMEDIO********************************************************************************************************************************************
                    
                    if(serror<listafilastam){
                    comp= Long.parseLong(listacor.get(1).get(serror).toString());}
                    else{
                        comp=-1;
                    }
                    if(i==comp){
                        serror++;
                    }else{
                        promD+=Double.parseDouble(valorHijo);
                        //MAX*************************************************************************************************************************************************
                        aux=Double.parseDouble(valorHijo);
                        if(i==0){
                            valMax=aux;
                            valMin=aux;
                        }else{
                            if(valMax<aux){
                                valMax=aux;
                            }
                            //MIN*************************************************************************************************************************************************
                            if(valMin>aux){
                                valMin=aux;
                            }
                        }
                    }  
                }
                //MODA************************************************************************************************************************************************
                int vecesQueSeRepite = 0;
                for (int x = 0; x < tamFilas; x++) {
                    List<Tag> etqHijos2=new ArrayList(filas.get(x).getTagsHijos());
                    String valorHijo2 = etqHijos2.get(c.getNum()).getContenido().trim();
                    if (valorHijo.equals(valorHijo2)) {
                        vecesQueSeRepite++;
                    }
                }
                if (vecesQueSeRepite > maximaVecesQueSeRepite) {
                    moda = valorHijo;
                    maximaVecesQueSeRepite = vecesQueSeRepite;
                }

            }
            if(c.getTipo().contains("numero")){
                promD=promD/tamFilas;
                if(c.getTipo().contains("flotante")){
                    c.setMax(valMax+"");
                    c.setMin(valMin+"");
                    c.setPromedio(promD+"");
                }else{
                    c.setMax(((long)valMax)+"");
                    c.setMin(((long)valMin)+"");
                    c.setPromedio(((long)promD)+"");
                }
                c.setModa(moda);
            }else{
                c.setMax("Valor no numerico");
                c.setMin("Valor no numerico");
                c.setPromedio("Valor no numerico");
                c.setModa(moda);
            }
            columnas.add(c);
        }
    }
    
    public void corregir() throws XMLStreamException, IOException{
        int contador=0,uno=1;
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = null;
        xtw = xof.createXMLStreamWriter(new FileWriter(this.ruta));
        for (Object a : listacor.get(1)) {
            System.out.println(a);
        }
        System.out.println(this.ruta);
        xtw.writeStartDocument("utf-8", "1.0");
        xtw.writeCharacters("\n");
        xtw.writeStartElement("Archivo");
        
        for (int x = 0; x < tamFilas; x++) {
            xtw.writeCharacters("\n");
            xtw.writeStartElement("row");
            for (int i = 0; i < numCol; i++) {
                
                etqHijos=new ArrayList(filas.get(x).getTagsHijos());
                valorHijo = etqHijos.get(columnas.get(i).getNum()).getContenido();
                xtw.writeCharacters("\n");
                xtw.writeStartElement(etqHijos.get(columnas.get(i).getNum()).getNombre());
                if( contador<listacor.get(listacor.size()-1).size() ){
                    if((x==Integer.parseInt(listacor.get(1).get(contador).toString())) && (i==Integer.parseInt(listacor.get(2).get(contador).toString())) ){
                        System.out.println(listacor.get(listacor.size()-1).get(contador).toString());
                        if(uno==Integer.parseInt(listacor.get(listacor.size()-1).get(contador).toString())){
                            System.out.println(listacor.get(3).get(contador).toString().contains("numero"));
                            if(listacor.get(3).get(contador).toString().contains("numero")){
                                xtw.writeCharacters(this.columnas.get(Integer.parseInt(listacor.get(2).get(contador).toString())).getPromedio());
                            }else{
                                xtw.writeCharacters(this.columnas.get(Integer.parseInt(listacor.get(2).get(contador).toString())).getModa());
                            }
                        }else{
                            xtw.writeCharacters(valorHijo);
                        }
                        contador++;
                    }else{
                        xtw.writeCharacters(valorHijo);
                    }
                }else{
                    xtw.writeCharacters(valorHijo);
                }
                xtw.writeEndElement();
                
            }
            xtw.writeCharacters("\n");
            xtw.writeEndElement();
            
        }
        xtw.writeCharacters("\n");
        xtw.writeEndElement();
        xtw.writeEndDocument();
        
        xtw.flush();
        xtw.close();
        System.out.println("Done");
       
    }
    
    
}
