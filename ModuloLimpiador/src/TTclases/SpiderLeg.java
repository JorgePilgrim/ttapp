/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TTclases;


import java.io.*;
import java.util.*;
import org.jsoup.*;
import org.jsoup.nodes.*;
import org.jsoup.select.*;
import java.net.*;
import java.text.Normalizer;
import java.util.regex.Pattern;

/**
 *
 * @author ALAN
 */
public class SpiderLeg {
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 "
            + "(KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
    protected List<String> links = new LinkedList<String>();
    private Document htmlDocument;


    /**
     * 
     * 
     * @param url
     *            - The URL to visit
     * @return whether or not the crawl was successful
     */
    public boolean crawl(String url)
    {
        try
        {
            Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
            Document htmlDocument = connection.get();
            this.htmlDocument = htmlDocument;
            if(connection.response().statusCode() == 200)
            {
                System.out.println("\n**Visitando** sitio: " + url);
            }
            if(!connection.response().contentType().contains("text/html"))
            {
                System.out.println("**Fallo** no es HTML");
                return false;
            }
            Elements linksOnPage = htmlDocument.select("a[href]");
            
            for(Element link : linksOnPage)
            {
                
                this.links.add(link.absUrl("href"));
            }
            
            return true;
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
            return false;
        }
    }

    public boolean searchForCSV(List<String> links, String url)
    {
        List<String> csv = new LinkedList<>();
        List<String> nombres = new LinkedList<>();
        String [] partes;
        File cPrincipal=new File("ARCHIVOS");
        cPrincipal.mkdir();
        File sitio=new File(cPrincipal+"/"+nombreCarpeta(url));
        sitio.mkdir();
        String folder = sitio.getPath()+"/CSV/";
        System.out.println("Archivos CSV");
        for(String link : links)
        {
            if(link.endsWith(".csv"))
            {
                csv.add(link);
                System.out.println(link);
                partes=link.split("/");
                nombres.add(partes[partes.length-1]);
            }else if(link.endsWith("CSV")){
                csv.add(link);
                System.out.println(link);
                nombres.add("CSV_Archivo "+csv.size()+".csv");
            }
            
        }
        
        File dir = new File(folder);
        dir.mkdir();
        
        for (int i = 0; i < csv.size(); i++) {
            File file = new File(folder + nombres.get(i));
            try{
            URLConnection conn = new URL(csv.get(i)).openConnection();
            conn.connect();
            System.out.println("\nempezando descarga: \n");
            System.out.println(">> URL: " + csv.get(i));
            System.out.println(">> Nombre: " + nombres.get(i));
            InputStream in = conn.getInputStream();
            OutputStream out = new FileOutputStream(file);
            
            int b = 0;
            char []ch;
            String normal;
            while (b != -1) {
              b = in.read();
              if (b != -1){
                ch = Character.toChars(b);
                normal=normalizar(""+ch[0]);
                out.write(normal.getBytes());
              }
            }
            
            out.close();
            in.close();
            
            CSV2XML xmlprueba=new CSV2XML();
                xmlprueba.convertFile(file.getPath(), file.getPath().replace(".csv",".xml" ), ",");
            }
            catch(IOException e) {
            e.printStackTrace();
          }
            
        }
        
        return !csv.isEmpty();
    }
    
    public boolean searchForJSON(List<String> links, String url)
    {
        List<String> json = new LinkedList<>();
        List<String> nombres = new LinkedList<>();
        String [] partes;
        String folder = "ARCHIVOS/"+nombreCarpeta(url)+"/JSON/";
        System.out.println("Archivos JSON");
        for(String link : links)
        {
            if(link.endsWith(".json"))
            {
                json.add(link);
                System.out.println(link);
                partes=link.split("/");
                nombres.add(partes[partes.length-1]);
            }else if(link.endsWith("JSON")){
                json.add(link);
                System.out.println(link);
                nombres.add("JSON_Archivo "+json.size()+".json");
            }
            
        }
        
        File dir = new File(folder);
        dir.mkdir();
        
        for (int i = 0; i < json.size(); i++) {
            File file = new File(folder + nombres.get(i));
            try{
            URLConnection conn = new URL(json.get(i)).openConnection();
            conn.connect();
            System.out.println("\nempezando descarga: \n");
            System.out.println(">> URL: " + json.get(i));
            System.out.println(">> Nombre: " + nombres.get(i));
            InputStream in = conn.getInputStream();
            OutputStream out = new FileOutputStream(file);
            
            int b = 0;
            char []ch;
            String normal;
            while (b != -1) {
              b = in.read();
              if (b != -1){
                ch = Character.toChars(b);
                normal=normalizar(""+ch[0]);
                out.write(normal.getBytes());
              }
            }
            
            out.close();
            in.close();
            
            JSON2XML jsonc = new JSON2XML();
            String jsonFile = jsonc.readFile(file.getPath());
                System.out.println(file.getPath());
            String xml = jsonc.convert(jsonFile, "root");
            jsonc.writeFile(file.getPath().replace(".json", ".xml"), xml);
                System.out.println("Convertido");
            }
            catch(IOException e) {
                System.out.println(e.getMessage());
          }
            
        }
        
        return !json.isEmpty();
    }
    
    public boolean searchForXML(List<String> links, String url)
    {
        List<String> xml = new LinkedList<>();
        List<String> nombres = new LinkedList<>();
        String [] partes;
        String folder = "ARCHIVOS/"+nombreCarpeta(url)+"/XML/";
        System.out.println("Archivos XML");
        for(String link : links)
        {
            if(link.endsWith("ml"))
            {
                xml.add(link);
                System.out.println(link);
                partes=link.split("/");
                nombres.add(partes[partes.length-1]);
            }else if(link.endsWith("ML")){
                xml.add(link);
                System.out.println(link);
                nombres.add("XML_Archivo "+xml.size()+".xml");
            }
            
        }
        
        File dir = new File(folder);
        dir.mkdir();
        
        for (int i = 0; i < xml.size(); i++) {
            File file = new File(folder + nombres.get(i));
            try{
            URLConnection conn = new URL(xml.get(i)).openConnection();
            conn.connect();
            System.out.println("\nempezando descarga: \n");
            System.out.println(">> URL: " + xml.get(i));
            System.out.println(">> Nombre: " + nombres.get(i));
            InputStream in = conn.getInputStream();
            OutputStream out = new FileOutputStream(file);
            
            int b = 0;
            char []ch;
            String normal;
            while (b != -1) {
              b = in.read();
              if (b != -1){
                ch = Character.toChars(b);
                normal=normalizar(""+ch[0]);
                out.write(normal.getBytes());
              }
            }
            
            out.close();
            in.close();
            }
            catch(IOException e) {
                System.out.println(e.getMessage());
          }
            
        }
        
        return !xml.isEmpty();
    }

    public String nombreCarpeta(String url){
        String carpeta="";
        
        //NOMBRE CARPETAS
        carpeta= url.replace("/", "-");
        carpeta= carpeta.replace("\\", "-");
        carpeta= carpeta.replace("?", "-");
        carpeta= carpeta.replace(":", "-");
        carpeta= carpeta.replace("*", "-");
        carpeta= carpeta.replace("\"", "-");
        carpeta= carpeta.replace("<", "-");
        carpeta= carpeta.replace(">", "-");
        carpeta= carpeta.replace("|", "-");
        
        return carpeta;
    }

    public List<String> getLinks()
    {
        return this.links;
    }
    
    public static String normalizar(String input) {
        String normalized = Normalizer.normalize(input, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\P{ASCII}+");
        return pattern.matcher(normalized).replaceAll("");
    }
    
}
