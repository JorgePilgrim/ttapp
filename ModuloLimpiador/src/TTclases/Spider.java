/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TTclases;

import java.util.*;

/**
 *
 * @author ALAN
 */
public class Spider {
    private int maxpages;
    private String currentUrl;
    public List<String> boolTiposArchivos= new ArrayList<>();
    private Set<String> pagesVisited = new HashSet<String>();
    public List<String> pagesToVisit = new LinkedList<>();

    public void extract()
  {
      int nextTipos=0;
      maxpages=this.pagesVisited.size();
      currentUrl=this.pagesToVisit.get(0);
      System.out.println("Visitando "+currentUrl);
      String uno = "1";
      while(this.pagesVisited.size() <= maxpages)
      {
          
          SpiderLeg leg = new SpiderLeg();
          
          leg.crawl(currentUrl);      
          
          //CSV 
          if(uno.equals(""+boolTiposArchivos.get(nextTipos).charAt(0))){
            boolean success = leg.searchForCSV(leg.links,currentUrl);
            if(success)
            {
                System.out.println("CSV en "+currentUrl+" "+success);
            }
            else{
                System.out.println("No hay archivos CSV");
            }
          }
          
          //JSON
          if(uno.equals(""+boolTiposArchivos.get(nextTipos).charAt(1))){
            boolean success2 = leg.searchForJSON(leg.links,currentUrl);
            if(success2)
            {
                System.out.println(String.format("JSON en "+currentUrl+" "+success2));
            }
            else{
                System.out.println("No hay archivos JSON");
            }
          }
          
          //XML
          if(uno.equals(""+boolTiposArchivos.get(nextTipos).charAt(2))){
              //System.out.println("entre");
            boolean success3 = leg.searchForXML(leg.links,currentUrl);
            if(success3)
            {
                System.out.println(String.format("XML en "+currentUrl+" "+success3));
            }
            else{
                System.out.println("No hay archivos XML");
            }
          }
          
          nextTipos++;
          currentUrl = this.nextUrl();
      }
  }
    
    
    private String nextUrl()
  {
      String nextUrl;
      do
      {
          nextUrl = this.pagesToVisit.remove(0);
      } while(this.pagesVisited.contains(nextUrl));
      this.pagesVisited.add(nextUrl);
      return nextUrl;
  }
    
}
