/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TTclases;

import java.sql.*;

/**
 *
 * @author ALAN
 */
public class Conexion {
    private String url;
    private String driver;
    private String user;
    private String pass;
    private Connection con;
    
    public Conexion(){
        this.driver="oracle.jdbc.driver.OracleDriver";
        this.url="jdbc:oracle:thin:@localhost:1521:xe";
        this.user="tt";
        this.pass="2016";
        this.con=null;
        try{
            Class.forName(driver);
            con=DriverManager.getConnection(url,user,pass);
            if(con!=null)
                System.out.println("Conexion realizada.");
            
        }catch(ClassNotFoundException | SQLException e){
            System.out.println("Error al conectar");
            System.out.println(e.getMessage());
        }
    }
    
    public Connection conectar(){
        return con;
    }
    public void close(){
            con=null;
    }
}
