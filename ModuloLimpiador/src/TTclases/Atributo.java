
package TTclases;

/**
 *
 * @author ALAN
 */
public class Atributo {
    String tipo;
    String nombre;
    String promedio;
    String max;
    String min;
    String err;
    String moda;
    String totalcor;
    int num;

    public Atributo() {
    }

    public String getTipo() {
        return tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPromedio() {
        return promedio;
    }

    public String getMax() {
        return max;
    }

    public String getMin() {
        return min;
    }

    public String getErr() {
        return err;
    }

    public String getModa() {
        return moda;
    }

    public String getTotalcor() {
        return totalcor;
    }

    public int getNum() {
        return num;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPromedio(String promedio) {
        this.promedio = promedio;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public void setModa(String moda) {
        this.moda = moda;
    }

    public void setTotalcor(String totalcor) {
        this.totalcor = totalcor;
    }

    public void setNum(int num) {
        this.num = num;
    }
    
}
