package TTclases;

import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
 
public class PDF {
    private Archivo a;
 
    public PDF(Archivo a){
        this.a=a;
    }
 
    public void createPdf(File dest) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        Font titulo = FontFactory.getFont(FontFactory.TIMES, 20, Font.BOLD);
        Font subtitulo = FontFactory.getFont(FontFactory.TIMES, 16, Font.NORMAL);
        
        Font datos = FontFactory.getFont(FontFactory.TIMES, 12, Font.NORMAL);
        Chunk chunk = new Chunk("Habilitador de datos para análisis en Big Data\nTT: "
                + "2016-A002\n\nReporte de datos", titulo);
        Chapter chapter = new Chapter(new Paragraph(chunk), 1);
        chapter.setNumberDepth(0);
        chapter.add(new Paragraph("\n", subtitulo));
        chapter.add(new Paragraph("Información general", subtitulo));
        chapter.add(new Paragraph("Nombre archivo: "+a.getNombre(), datos));
        chapter.add(new Paragraph("URL origen: "+a.getSitio(), datos));
        chapter.add(new Paragraph("Registros totales: "+a.getTamFilas(), datos));
        chapter.add(new Paragraph("Atributos totales: "+a.getNumCol(), datos));
        chapter.add(new Paragraph("Errores totales: "+a.gettotalerr(), datos));
        chapter.add(new Paragraph("\n", subtitulo));
        chapter.add(new Paragraph("Información detallada", subtitulo));
        
        
        List<Atributo> columnas = a.getColumnas();
        for (Atributo c: columnas) {
            chapter.add(new Paragraph("Nombre de atributo: "+c.getNombre(), datos));
            chapter.add(new Paragraph("Tipo de dato: "+c.getTipo(), datos));
            chapter.add(new Paragraph("Valor promedio: "+c.getPromedio(), datos));
            chapter.add(new Paragraph("Valor máximo: "+c.getMax(), datos));
            chapter.add(new Paragraph("Valor mínimo: "+c.getMin(), datos));
            chapter.add(new Paragraph("Moda: "+c.getModa(), datos));
            chapter.add(new Paragraph("Errores: "+c.getErr(), datos));
            chapter.add(new Paragraph("\n", datos));
        }
        
        document.add(chapter);
        document.close();
    }
}