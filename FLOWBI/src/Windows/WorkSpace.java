/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Windows;

import flowbi.ActionListeners;
import flowbi.Algorithm;
import flowbi.DataBaseTask;
import flowbi.Line;
import flowbi.LogicInterface;
import flowbi.Task;
import flowbi.WorkFlow;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ToolTipManager;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author Jorge
 */
public class WorkSpace extends javax.swing.JPanel {

    /**
     * Creates new form WorkSpace
     */
    public WorkSpace() {
        
        //constructor
        
        tarea = null;
        db = null;
        algorithm = null;
        Scale  = new LogicInterface();
        actions = new ActionListeners();
        tareas = new ArrayList<>();
        lines = new ArrayList<>();
        algorithms = new ArrayList<>();
        wf = new ArrayList<>();

        ancho = -1;
        alto = 64;
        iconSize = 78;
        widthPos = 0;
        lineCounter = 0;
        taskCounter = 0;
        
        Selected = null;
        drawLine = false;
        m2 = new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent me) {
                //cuando se arrastra el raton mandamos a llamar las siguientes funciones y refrescamos el panel
                widthPos = actions.DraggedActionDone(tareas,lines, me, widthPos);
                PanelWorkSpace.updateUI();
            }

            @Override
            public void mouseMoved(MouseEvent me) {
                
            }
        };
        m1 = new MouseListener(){
            @Override
            public void mouseClicked(MouseEvent me) {
                if(me.getClickCount() == 1){
                    if(drawLine){//si el boton para conectar tareas fue presionado
                        if (Selected == null) {
                            Selected = actions.TaskSelected(tareas, me);
                            System.out.println("Seleccion 1");
                        }else{
                            System.out.println("Seleccion 2");
                            tarea = actions.TaskSelected(tareas, me);//obtenemos la segunda tarea seleccionada
                            lineCounter = actions.VisualConection(tareas, Selected, tarea, lines, lineCounter);//conectamos las lineas
                            PanelWorkSpace.updateUI();//actualizamos el panel
                            //reseteamos variables
                            Selected = null;
                            tarea = null;
                            drawLine = false;
                        }   
                }else{//si no es conexion solo asignamos a la variable selected su target
                    Selected = actions.TaskSelected(tareas, me);
                }
                    
                }
                if (me.getClickCount() == 2) {
                    
                    Selected = actions.TaskSelected(tareas, me);
                    String name = Selected.getTareaID();
                    
                    if (name.contains("DB")) {
                        String ans = Scale.ConfigDB(name);
                        if (ans != "NA") {
                            db.setRoute(ans);
                            db.setPadre(name);
                        } else{
                            JOptionPane.showConfirmDialog(null,
                                    "No hay ningun archivo seleccionando",
                                    "Configuration of "+name, JOptionPane.DEFAULT_OPTION);
                        }
                    }
                    if (name.contains("Algorithm")) {
                        boolean newAlgo = true;
                        String algo = "";
                        for (Algorithm t:algorithms) {
                            
                            if (t.getPadre() == name) {
                                algo = Scale.ConfigAlgorithm(name);
                                
                                if (algo.contains("K-nn")) {
                                    String ans = Scale.ConfigKnnTraining(name);
                                    if (ans != "NA") {
                                        
                                        try {
                                            t.setName(algo,ans);
                                            newAlgo = false;
                                            break;
                                        } catch (XMLStreamException ex) {
                                            Logger.getLogger(WorkSpace.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (IOException ex) {
                                            Logger.getLogger(WorkSpace.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    } else{
                                        JOptionPane.showConfirmDialog(null,
                                                "y ningun archivo seleccionando",
                                                "Configuration of "+name, JOptionPane.DEFAULT_OPTION);
                                    }  
                                }
                                if (algo.contains("Apriori")) {
                                    t.setName(algo);
                                    newAlgo = false;
                                    break;
                                }                                
                            }
                        }
                        if (newAlgo) {
                            algo = Scale.ConfigAlgorithm(name);
                                
                                if (algo.contains("K-nn")) {
                                    String ans = Scale.ConfigKnnTraining(name);
                                    
                                    if (ans != "NA") {
                                        try {
                                            algorithm = new Algorithm(algo,name,ans);
                                            algorithms.add(algorithm);
                                            algorithm = null;
                                        } catch (XMLStreamException ex) {
                                            Logger.getLogger(WorkSpace.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (IOException ex) {
                                            Logger.getLogger(WorkSpace.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    
                                    else{
                                        JOptionPane.showConfirmDialog(null,
                                                "y ningun archivo seleccionando",
                                                "Configuration of "+name, JOptionPane.DEFAULT_OPTION);
                                    }  
                                }
                                
                                if (algo.contains("Apriori")) {
                                    algorithm = new Algorithm(algo,name);
                                    algorithms.add(algorithm);
                                    algorithm = null;
                                }
                        }
                        
                    }
                     if (name.contains("Validation")) {
                        
                    }
                    if (name.contains("Graphic")) {
                        
                    }
                    Selected = null;
                 } 
            }

            @Override
            public void mousePressed(MouseEvent me) {
                
            }

            @Override
            public void mouseReleased(MouseEvent me) {
                
                
            }

            @Override
            public void mouseEntered(MouseEvent me) {}

            @Override
            public void mouseExited(MouseEvent me) {}
        };
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ToolBar = new javax.swing.JToolBar();
        LabButtDB = new javax.swing.JLabel();
        LabButtAlgorithm = new javax.swing.JLabel();
        LabButtValidation = new javax.swing.JLabel();
        LabButtGraphic = new javax.swing.JLabel();
        LabButtConnection = new javax.swing.JLabel();
        LabButtEraser = new javax.swing.JLabel();
        PanelWorkSpace = new javax.swing.JPanel(){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g2d = (Graphics2D)g;
                actions.paintLines(lines, g2d);
            }
        };

        setBackground(new java.awt.Color(48, 70, 135));
        setForeground(new java.awt.Color(32, 47, 90));

        ToolBar.setBackground(new java.awt.Color(80, 80, 80));
        ToolBar.setBorder(null);
        ToolBar.setOrientation(javax.swing.SwingConstants.VERTICAL);
        ToolBar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        ToolTipManager.sharedInstance().setDismissDelay(11000);
        LabButtDB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabButtDB.setIcon(Scale.ScaleImage(new javax.swing.ImageIcon(getClass().getResource("/Images/DBSF.png")),alto,ancho));
        LabButtDB.setMaximumSize(new java.awt.Dimension(80, 80));
        LabButtDB.setMinimumSize(new java.awt.Dimension(80, 80));
        LabButtDB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabButtDBMouseClicked(evt);
            }
        });
        LabButtDB.setToolTipText("<html>"
            +"Base de datos."
            +"<br>"
            +"<br>"
            +"Esta tarea es indispensable y debe ser la"
            +"<br>"
            +"primera dentro del workflow, a través de esta"
            +"<br>"
            +"comenzaras con la selección, recolección y "
            +"<br>"
            +"tratamiento de datos dentro del proceso de KDD."
            +"</html>");
        ToolBar.add(LabButtDB);

        LabButtAlgorithm.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabButtAlgorithm.setIcon(Scale.ScaleImage(new javax.swing.ImageIcon(getClass().getResource("/Images/AlgorithmSF.png")),alto,ancho));
        LabButtAlgorithm.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LabButtAlgorithm.setMaximumSize(new java.awt.Dimension(80, 80));
        LabButtAlgorithm.setMinimumSize(new java.awt.Dimension(80, 80));
        LabButtAlgorithm.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabButtAlgorithmMouseClicked(evt);
            }
        });
        LabButtAlgorithm.setToolTipText("<html>"
            + "Algoritmo."
            +"<br>"
            +"<br>"
            +"La tarea algoritmo será siempre la segunda"
            +"<br>"
            +"dentro del workflow y permite elegir la tarea de"
            +"<br>"
            +"minería de datos según el algoritmo seleccionado,"
            +"<br>"
            +"así como la aplicación de este, es decir, llevar a"
            +"<br>"
            +"cabo la minería de datos dentro del proceso de KDD."
            + "</html>");
        ToolBar.add(LabButtAlgorithm);

        LabButtValidation.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabButtValidation.setIcon(Scale.ScaleImage(new javax.swing.ImageIcon(getClass().getResource("/Images/ValidationSF.png")),alto,ancho));
        LabButtValidation.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LabButtValidation.setMaximumSize(new java.awt.Dimension(80, 80));
        LabButtValidation.setMinimumSize(new java.awt.Dimension(80, 80));
        LabButtValidation.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabButtValidationMouseClicked(evt);
            }
        });
        LabButtValidation.setToolTipText("<html>"
            +"Validación."
            +"<br>"
            +"<br>"
            +"La tarea validación permite evaluar los"
            +"<br>"
            +"resultados de un análisis y garantizar que"
            +"<br>"
            +"son independientes de la partición entre datos de"
            +"<br>"
            +"entrenamiento y prueba (según sea el caso, es decir,"
            +"<br>"
            +"el algoritmo seleccionado); dicha tarea es"
            +"<br>"
            +"opcional dentro del workflow y dependiente del"
            +"<br>"
            +"algoritmo seleccionado."
            + "</html>");
        ToolBar.add(LabButtValidation);

        LabButtGraphic.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabButtGraphic.setIcon(Scale.ScaleImage(new javax.swing.ImageIcon(getClass().getResource("/Images/GraphicSF.png")),alto,ancho));
        LabButtGraphic.setToolTipText("<html>"
            +"Gráfico"
            +"<br>"
            +"<br>"
            +"La tarea gráfico representa la fase de"
            +"<br>"
            +"interpretación dentro del proceso de KDD y realiza"
            +"<br>"
            +"una representación de los datos analizados a través"
            +"<br>"
            +"de gráficos visuales."
            +"</html>");
        LabButtGraphic.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LabButtGraphic.setMaximumSize(new java.awt.Dimension(80, 80));
        LabButtGraphic.setMinimumSize(new java.awt.Dimension(80, 80));
        LabButtGraphic.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabButtGraphicMouseClicked(evt);
            }
        });
        ToolBar.add(LabButtGraphic);

        LabButtConnection.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabButtConnection.setIcon(Scale.ScaleImage(new javax.swing.ImageIcon(getClass().getResource("/Images/Connect.png")),alto,ancho));
        LabButtConnection.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LabButtConnection.setMaximumSize(new java.awt.Dimension(80, 80));
        LabButtConnection.setMinimumSize(new java.awt.Dimension(80, 80));
        LabButtConnection.setToolTipText("<html>"
            +"Ligar Tarea."
            +"<br>"
            +"<br>"
            +"Utiliza este componente para ligar tareas y dar una"
            +"<br>"
            +"secuencia de orden y lógica en la creación del"
            +"<br>"
            +"workflow."
            + "</html>"
        );
        LabButtConnection.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabButtConnectionMouseClicked(evt);
            }
        });
        ToolBar.add(LabButtConnection);

        LabButtEraser.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabButtEraser.setIcon(Scale.ScaleImage(new javax.swing.ImageIcon(getClass().getResource("/Images/Delete.png")),alto,ancho));
        LabButtEraser.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LabButtEraser.setMaximumSize(new java.awt.Dimension(80, 80));
        LabButtEraser.setMinimumSize(new java.awt.Dimension(80, 80));
        LabButtEraser.setToolTipText("<html>"
            +"Eliminar tarea"
            +"<br>"
            +"<br>"
            +"Selecciona la tarea a eliminar del workflow y"
            +"<br>"
            +"en seguida este botón para hacerlo."
            + "</html>");
        LabButtEraser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabButtEraserMouseClicked(evt);
            }
        });
        ToolBar.add(LabButtEraser);

        PanelWorkSpace.setBackground(new java.awt.Color(205, 205, 205));
        PanelWorkSpace.setPreferredSize(new java.awt.Dimension(1000, 360));

        javax.swing.GroupLayout PanelWorkSpaceLayout = new javax.swing.GroupLayout(PanelWorkSpace);
        PanelWorkSpace.setLayout(PanelWorkSpaceLayout);
        PanelWorkSpaceLayout.setHorizontalGroup(
            PanelWorkSpaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 949, Short.MAX_VALUE)
        );
        PanelWorkSpaceLayout.setVerticalGroup(
            PanelWorkSpaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(ToolBar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(PanelWorkSpace, javax.swing.GroupLayout.DEFAULT_SIZE, 949, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ToolBar, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
            .addComponent(PanelWorkSpace, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void LabButtDBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabButtDBMouseClicked
        if(tareas.isEmpty()){
            try {
            // si es el primer elemento se crea la tarea de DB
            
            tarea = new Task("DB",0,widthPos,PanelWorkSpace.getHeight()/2,iconSize,m1,m2);
            tarea.setTareaID("DB"+taskCounter);
            taskCounter++;
            widthPos = widthPos+iconSize+8;
            PanelWorkSpace.add(tarea.getTask());
            tareas.add(tarea);
            
            String ans = Scale.ConfigDB(tarea.getTareaID());
            if (ans != "NA") {
                            db = new DataBaseTask(ans,tarea.getTareaID());
                        } else{
                            db = new DataBaseTask(tarea.getTareaID());
                            JOptionPane.showConfirmDialog(null,
                                    "No hay ningun archivo seleccionando",
                                    "Configuration for "+tarea.getTareaID(), JOptionPane.DEFAULT_OPTION);
                        }
            
            PanelWorkSpace.updateUI();
            
            } catch (XMLStreamException ex) {
                Logger.getLogger(WorkSpace.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(WorkSpace.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }else{
            JOptionPane.showConfirmDialog(null, 
                "No puedes tener dos inicios en un mismo flujo", "Ok!", JOptionPane.DEFAULT_OPTION);
            
        }
        
        
    }//GEN-LAST:event_LabButtDBMouseClicked

    private void LabButtAlgorithmMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabButtAlgorithmMouseClicked
        if(!tareas.isEmpty()){ // si no es el primer elemento puedes crear la tarea algoritmo
            tarea = new Task("Algorithm",0,widthPos,PanelWorkSpace.getHeight()/2,iconSize,m1,m2);
            tarea.setTareaID("Algorithm"+taskCounter);
            taskCounter++;
            widthPos = widthPos+iconSize+8;
            PanelWorkSpace.add(tarea.getTask());
            tareas.add(tarea);
            PanelWorkSpace.updateUI();
        }else{
            
            JOptionPane.showConfirmDialog(null, 
                "Esta tarea no puede ser tu primer componente", "Ok!", JOptionPane.DEFAULT_OPTION);
        }
    }//GEN-LAST:event_LabButtAlgorithmMouseClicked

    private void LabButtValidationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabButtValidationMouseClicked
        if(!tareas.isEmpty()){//si no es la primera tarea se puede crear  la tarea validacion
            tarea = new Task("Validation",0,widthPos,PanelWorkSpace.getHeight()/2,iconSize,m1,m2);
            tarea.setTareaID("Validation"+taskCounter);
            taskCounter++;
            widthPos = widthPos+iconSize+8;
            PanelWorkSpace.add(tarea.getTask());
            tareas.add(tarea);
            PanelWorkSpace.updateUI();
            
        }else{
            JOptionPane.showConfirmDialog(null, 
                "Esta tarea no puede ser tu primer componente", "Ok!", JOptionPane.DEFAULT_OPTION);
        }
    }//GEN-LAST:event_LabButtValidationMouseClicked

    private void LabButtGraphicMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabButtGraphicMouseClicked
        if(!tareas.isEmpty()){ // si no es la primer tarea puedes crear una tarea graphic
            tarea = new Task("Graphic",0,widthPos,PanelWorkSpace.getHeight()/2,iconSize,m1,m2);
            tarea.setTareaID("Graphic"+taskCounter);
            taskCounter++;
            widthPos = widthPos+iconSize+8;
            PanelWorkSpace.add(tarea.getTask());
            tareas.add(tarea);
            PanelWorkSpace.updateUI();
        }else{
            JOptionPane.showConfirmDialog(null, 
                "Esta tarea no puede ser tu primer componente", "Ok!", JOptionPane.DEFAULT_OPTION);
        }
    }//GEN-LAST:event_LabButtGraphicMouseClicked

    private void LabButtConnectionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabButtConnectionMouseClicked
        Selected = null;
        drawLine = true;//cambia el estado de la bandera
         JOptionPane.showConfirmDialog(null,
                                    "Selecciona dos tareas a unir",
                                    "Unir Tareas", JOptionPane.DEFAULT_OPTION);
        PanelWorkSpace.updateUI();
    }//GEN-LAST:event_LabButtConnectionMouseClicked

    private void LabButtEraserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabButtEraserMouseClicked
        if(Selected != null){
            //elimina la tarea selecionada.
            PanelWorkSpace.remove(actions.EraseSelected(tareas, lines, Selected, g2d));
            PanelWorkSpace.updateUI();
            Selected = null;
        }
    }//GEN-LAST:event_LabButtEraserMouseClicked
    
    public void playWorkFlow(){
        if (db.getCSV()!= null) {
            wf=Scale.CreateWorkFlows(db, algorithms, tareas,1);
        for (WorkFlow w:wf) {
            w.Play();
        }
        
        }else{
            JOptionPane.showConfirmDialog(null, 
                "No puedes iniciar tu flujo sin un archivo cargado", "Ok!", JOptionPane.DEFAULT_OPTION);
        }
        
    }
    
    
    //variables para manejo de la interfaz
    private int ancho;//cambia el ancho de los botones
    private int alto;//cambia el alto de los botones
    private int iconSize;//tamaño de las tareas
    private int widthPos;// posicion donde se crean las tareas en el eje x
    private int lineCounter;//contador de lineas creadas
    private int taskCounter;
    private MouseListener m1;//listener de mouse para un click
    private MouseMotionListener m2;//listener de mouse para los drag
    
    private LogicInterface Scale;//objeto de escala de imagenes
    private ActionListeners actions;//objeto de tareas de listeners
    private DataBaseTask db;//objeto de tarea DB
    private List<Algorithm> algorithms;
    private Algorithm algorithm;
    private List<WorkFlow> wf;//objeto workflow
    private Task tarea; // objeto tipo task
    private Task Selected; //objeto tipo task
    private List<Task> tareas; // lista de tasks creadas
    private List<Line> lines;// lista de lineas creadas
    
    private boolean drawLine;//bandera de conexion.
    Graphics2D g2d;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabButtAlgorithm;
    private javax.swing.JLabel LabButtConnection;
    private javax.swing.JLabel LabButtDB;
    private javax.swing.JLabel LabButtEraser;
    private javax.swing.JLabel LabButtGraphic;
    private javax.swing.JLabel LabButtValidation;
    private javax.swing.JPanel PanelWorkSpace;
    private javax.swing.JToolBar ToolBar;
    // End of variables declaration//GEN-END:variables
}
