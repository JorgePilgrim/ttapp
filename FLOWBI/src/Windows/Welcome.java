/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Windows;

import flowbi.LogicInterface;

/**
 *
 * @author Jorge
 */
public class Welcome extends javax.swing.JPanel {
    
    /**
     * Creates new form Welcome
     */
    public Welcome() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelIPNImage = new javax.swing.JLabel();
        LabelESCOMImage = new javax.swing.JLabel();
        jLabelIPN = new javax.swing.JLabel();
        jLabelESCOM = new javax.swing.JLabel();
        jLabelGenerador = new javax.swing.JLabel();
        jLabelFlowBi = new javax.swing.JLabel();
        jLabelTT = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jTextPane1 = new javax.swing.JTextPane();

        setBackground(new java.awt.Color(48, 70, 135));
        setForeground(new java.awt.Color(32, 47, 90));
        setPreferredSize(new java.awt.Dimension(882, 414));

        LabelIPNImage.setIcon(Scale.ScaleImage(new javax.swing.ImageIcon(getClass().getResource("/Images/IPN.png")),-1,160));

        LabelESCOMImage.setIcon(Scale.ScaleImage(new javax.swing.ImageIcon(getClass().getResource("/Images/escom.png")),-1,160));

        jLabelIPN.setFont(new java.awt.Font("Cambria", 1, 24)); // NOI18N
        jLabelIPN.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelIPN.setText("Instituto Politécnico Nacional");

        jLabelESCOM.setFont(new java.awt.Font("Cambria", 1, 18)); // NOI18N
        jLabelESCOM.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelESCOM.setText("Escuela Superior De Cómputo");

        jLabelGenerador.setFont(new java.awt.Font("Meiryo", 1, 14)); // NOI18N
        jLabelGenerador.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelGenerador.setText("Generador de Flujos de Trabajo en Ambientes Big Data");

        jLabelFlowBi.setFont(new java.awt.Font("Meiryo UI", 1, 36)); // NOI18N
        jLabelFlowBi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFlowBi.setText("FlowBi");

        jLabelTT.setFont(new java.awt.Font("Meiryo", 1, 12)); // NOI18N
        jLabelTT.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTT.setText("TT2017-B109");

        jLabel1.setFont(new java.awt.Font("Meiryo", 1, 12)); // NOI18N
        jLabel1.setText("Autores:");

        jTextPane1.setBackground(new java.awt.Color(48, 70, 135));
        jTextPane1.setFont(new java.awt.Font("Meiryo", 1, 12)); // NOI18N
        jTextPane1.setText("Galindo Garcia Jose J.\nMartinez Guerrero Juan D.\nMundo Lopez Alejandro E.");
        jTextPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jTextPane1.setOpaque(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LabelIPNImage, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelIPN, javax.swing.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                    .addComponent(jLabelESCOM, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelGenerador, javax.swing.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                    .addComponent(jLabelFlowBi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(LabelESCOMImage, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(320, 320, 320)
                .addComponent(jLabelTT, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(296, 296, 296))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelIPN, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelESCOM, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelGenerador, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelFlowBi, javax.swing.GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE))
                    .addComponent(LabelESCOMImage, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LabelIPNImage, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabelTT, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                .addGap(97, 97, 97)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private LogicInterface Scale = new LogicInterface(); 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelESCOMImage;
    private javax.swing.JLabel LabelIPNImage;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelESCOM;
    private javax.swing.JLabel jLabelFlowBi;
    private javax.swing.JLabel jLabelGenerador;
    private javax.swing.JLabel jLabelIPN;
    private javax.swing.JLabel jLabelTT;
    private javax.swing.JTextPane jTextPane1;
    // End of variables declaration//GEN-END:variables
}
