package CleanModule;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jespxml.JespXML;
import org.jespxml.modelo.Tag;


public class Errores {

  //STRING BUILDER
  private StringBuilder sb;

  //LISTAS
  List<String> verror;
  List<String> ferror;
  List<String> cerror;
  List<String> cterror;
  List<String> errorxat;

  //PATTERN Y MATCHER
  private Pattern pattern;
  private Matcher matcher;

  //EXPRESIONES
  private final String REGEX_NUMERO_ENTERO="^([\\-\\+]?\\d+)";
  private final String REGEX_NUMERO_FLOTANTE="^([\\-\\+]?\\d+(\\.\\d+)?)";
  private final String REGEX_COORD = "^(([\\-\\+]?\\d+(\\.\\d+)?)\\s*,\\s*("
  + "[\\-\\+]?\\d+(\\.\\d+)?))";
  private final String REGEX_FECHA1 = "^((\\d{1,2})[/\\-]"
  + "((\\d{1,2})|((?i)((ene|feb|mar|abr|may|jun|jul|ago|sep|oct|nov|"
  + "dic)|(enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiemb"
  + "re|octubre|noviembre|diciembre))))"
  + "[/\\-](\\d{2,4}))";
  private final String REGEX_FECHA2 = "^((\\d{4})[/\\-]"
  + "((\\d{1,2})|((?i)((ene|feb|mar|abr|may|jun|jul|ago|sep|oct|nov|"
  + "dic)|(enero|febrero|marzo|abril|mayo|junio|julio|agosto|"
  + "septiembre|octubre|noviembre|diciembre))))"
  + "[/\\-](\\d{1,2}))";


  //VARIABLES EXTRA
  long error;
  long paso;
  String archivo;
  Tag raiz;
  long muestra;
  List<Tag> filas;
  List<String> valores;
  List<String> tiposCol;
  int columna;
  private List<String> reglas;
  public List<List> correciones;
  private List<String> nom_reg;
  long tamValores;
  long tamFilas;
  String valorHijo;
  List<Tag> etqHijos;
  private int nColumnas;
  private int nFilas;



  public Errores(String archivo) {
    this.archivo = archivo;
    tiposCol=new ArrayList<>();
    valores=new ArrayList<>();
    correciones=new ArrayList<>();

    verror=new ArrayList<>();
    ferror=new ArrayList<>();
    cerror=new ArrayList<>();
    cterror=new ArrayList<>();
    errorxat= new ArrayList<>();

    nom_reg=new ArrayList<>();
    nom_reg.add("numero");
    nom_reg.add("numero flotante");
    nom_reg.add("coordenadas");
    nom_reg.add("fecha");
    nom_reg.add("fecha");
    nom_reg.add("varchar");

    reglas = new ArrayList<>();
    reglas.add(REGEX_NUMERO_ENTERO);
    reglas.add(REGEX_NUMERO_FLOTANTE);
    reglas.add(REGEX_COORD);
    reglas.add(REGEX_FECHA1);
    reglas.add(REGEX_FECHA2);

    sb= new StringBuilder();
  }

  public void iniciar(){
    try {
      sb.append("Iniciando variables\n");
      JespXML xmlDoc = new JespXML(new File(archivo.trim()));

      raiz= xmlDoc.leerXML();
      filas=new ArrayList(raiz.getTagsHijos());

      //System.out.println(filas);

      tamFilas=filas.size();
      nColumnas=filas.get(0).getTagsHijos().size();
      muestra =(long)(filas.size()*0.20);
      correciones.add(verror);
      correciones.add(ferror);
      correciones.add(cerror);
      correciones.add(cterror);

    } catch (Exception e) {

      System.out.println(e);
    }
  }

  public void analizar(){
    for (int i = 0; i < nColumnas; i++) {
      columna=i;
      Tipo();
    }

  }

  public StringBuilder getSb() {
    return sb;
  }
  public List<String> getErrorxat() {
    return errorxat;
  }

  public List<String> getVerror() {
    return verror;
  }
  public List<List> getCorrecciones() {
    return correciones;
  }

  public List<String> getTiposCol() {
    return tiposCol;
  }

  public List<String> getNombresCol() {
    List<String> nombres = new ArrayList<>();

    for (Tag etq:etqHijos) {
      nombres.add(etq.getNombre());
    };
    return nombres;
  }

  private void Tipo(){
    error=0;
    paso=0;
    try {
      int aleatorio;
      //System.out.println("Cantidad de filas: "+filas.size());
      sb.append("Cantidad de filas: "+filas.size()+"\n");
      nFilas = filas.size();
      //System.out.println("Valor de la muestra: "+muestra);
      //System.out.println("COLUMNAS ->"+nColumnas);
      sb.append("Valor de la muestra: "+muestra+"\n"+"COLUMNAS ->"+nColumnas+"\n");

      //System.out.println("\n**********MUESTRA*************");
      sb.append("\n**********MUESTRA*************");
      for (int i = 0; i < muestra; i++) {
        aleatorio = (int) (Math.random() * muestra);
        etqHijos=new ArrayList(filas.get(aleatorio).getTagsHijos());
        valorHijo = etqHijos.get(columna).getContenido();
        //System.out.println("\n"+valorHijo);
        sb.append("\n"+valorHijo);
        valores.add(valorHijo);
      }
      tamValores=valores.size();
      expresiones();
    } catch (Exception e) {
      System.out.println("Error funcion tipo");
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
  }

  private void expresiones(){
    ArrayList<List> remplazo = new ArrayList<>();
    int isCad=0, tcol=0;
    long col_comp=(long) (muestra*0.75);
    long correcto;//20% DE LA MUESTRA
    if((muestra*0.20)<1){
      correcto=1;
    }else{
      correcto= (long)(muestra*0.20);
    }
    if(col_comp<1){
      col_comp=1;
    }

    for (int i = 0; i < 5; i++) {
      pattern = Pattern.compile(reglas.get(i));
      for (int j = 0; j < tamValores; j++) {
        matcher = pattern.matcher(valores.get(j).trim());
        //System.out.println(valores.get(j));
        //System.out.println(matcher.matches());
        sb.append("\n"+valores.get(j)+"\n Paso: "+matcher.matches()+"\n");
        if(matcher.matches() != Boolean.FALSE){
          paso++;
        }else{
          error++;
        }
        if(error>=correcto){
          break;
        }
        if(paso>=col_comp){
          /*System.out.println(paso+"-"+col_comp);
          System.out.println("Analizar todo el atributo con"
          + " regla "+nom_reg.get(i));
          sb.append("Analizar todo el atributo con regla"
          + " "+nom_reg.get(i));*/
          ccolumna(i);
          tiposCol.add(nom_reg.get(i));
          isCad=1;
          tcol=1;
          break;
        }

      }
      error=0;
      paso=0;
      if(tcol==1){
        break;
      }
    }

    if(isCad==0){
      tiposCol.add(nom_reg.get(5));
      cnulos();
    }
    remplazo.add(verror);
    remplazo.add(ferror);
    remplazo.add(cerror);
    remplazo.add(cterror);
    valores.clear();
  }

  private void ccolumna( int regla){
    int contador=0;

    //System.out.println("\n**********ATRIBUTO*************");
    sb.append("\n**********ATRIBUTO*************");
    pattern = Pattern.compile(reglas.get(regla));
    for (int i = 0; i < tamFilas; i++) {
      etqHijos=new ArrayList(filas.get(i).getTagsHijos());
      valorHijo = etqHijos.get(columna).getContenido();
      //System.out.println("\n"+valorHijo);
      sb.append("\n"+valorHijo);

      matcher = pattern.matcher(valorHijo.trim());
      if(matcher.matches() != Boolean.FALSE){
        //System.out.println("CORRECTO");
        sb.append("\nCORRECTO");
      }else{
        //System.out.println("INCORRECTO");
        sb.append("\nINCORRECTO");
        contador++;
        verror.add(valorHijo);
        ferror.add(""+i);
        cerror.add(""+columna);
        cterror.add(nom_reg.get(regla));

      }

    }
    errorxat.add(""+contador);

  }

  private void cnulos() {
    int contador=0;
    sb.append("\nBuscando valores nulos");
    for (int i = 0; i < tamFilas; i++) {
      etqHijos=new ArrayList(filas.get(i).getTagsHijos());
      valorHijo = etqHijos.get(columna).getContenido();
      if(valorHijo==null || valorHijo=="" || valorHijo==" "){
        verror.add(valorHijo);
        ferror.add(""+i);
        cerror.add(""+columna);
        cterror.add(nom_reg.get(5));
        contador++;
      }
    }
    sb.append("\nTermino de busqueda de valores nulos");
    errorxat.add(""+contador);
  }

  public int numRow(){
    return nFilas;
  }
}
