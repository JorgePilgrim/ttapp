package CleanModule;


import au.com.bytecode.opencsv.*;
import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CSV2XML {

  protected DocumentBuilderFactory domFactory = null;
  protected DocumentBuilder domBuilder = null;
  String valorerror;

  public CSV2XML() {
    try {
      domFactory = DocumentBuilderFactory.newInstance();
      domBuilder = domFactory.newDocumentBuilder();
    } catch (FactoryConfigurationError exp) {
      System.err.println(exp.toString());
    } catch (ParserConfigurationException exp) {
      System.err.println(exp.toString());
    } catch (Exception exp) {
      System.err.println(exp.toString());
    }

  }

  public int convertFile(String csvFileName, String xmlFileName,
  String delimiter) {

    int rowsCount = -1;
    BufferedReader csvReader;
    try {
      Document newDoc = domBuilder.newDocument();
      // Elemento raiz
      Element rootElement = newDoc.createElement("root");
      newDoc.appendChild(rootElement);
      // Leer csv
      csvReader = new BufferedReader(new FileReader(csvFileName));

      CSVReader reader = new CSVReader(csvReader);
      String[] nextLine;
      int line = 0;
      List<String> headers = new ArrayList<String>();
      while ((nextLine = reader.readNext()) != null) {

        if (line == 0) { // Cabecera
          for (String col : nextLine) {
            col=col.replace(" ", "_");
            headers.add(col);
          }
        } else { // Datos/filas
          Element rowElement = newDoc.createElement("row");
          rootElement.appendChild(rowElement);

          int col = 0;
          for (String value : nextLine) {
            String header = headers.get(col);
            Element curElement = newDoc.createElement(header);
            curElement.appendChild(newDoc.createTextNode(value.trim()));
            rowElement.appendChild(curElement);

            col++;
          }
        }
        line++;
      }

      FileWriter writer = null;

      try {

        writer = new FileWriter(new File(xmlFileName));

        TransformerFactory tranFactory = TransformerFactory.newInstance();
        Transformer aTransformer = tranFactory.newTransformer();
        aTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
        aTransformer.setOutputProperty(OutputKeys.METHOD, "xml");
        aTransformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        Source src = new DOMSource(newDoc);
        Result result = new StreamResult(writer);
        aTransformer.transform(src, result);

        writer.flush();

      } catch (Exception exp) {
        System.out.println(exp.getMessage());
      } finally {
        try {
          writer.close();
        } catch (Exception e) {
        }
      }

    } catch (IOException exp) {
      System.out.println(exp.getMessage());

    } catch (Exception exp) {
      System.err.println(exp.toString()+exp.getMessage()+valorerror);
    }
    return rowsCount;
  }

}
