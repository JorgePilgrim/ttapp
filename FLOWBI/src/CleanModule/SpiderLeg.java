/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package CleanModule;

import java.io.*;
import java.util.*;
import org.jsoup.*;
import org.jsoup.nodes.*;
import org.jsoup.select.*;
import java.net.*;
import java.text.Normalizer;
import java.util.regex.Pattern;

/**
*
* @author Jorge
*/
public class SpiderLeg {
  private String RuteCSV = "";
  private String RuteXML = "";
  private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 "
  + "(KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
  protected List<String> links = new ArrayList<String>();
  private Document htmlDocument;
  /**
  *
  *
  * @param url
  *            - The URL to visit
  * @return whether or not the crawl was successful
  */
  public String nombreCarpeta(String url){
    String carpeta="";

    //NOMBRE CARPETAS
    carpeta= url.replace("/", "-");
    carpeta= carpeta.replace("\\", "-");
    carpeta= carpeta.replace("?", "-");
    carpeta= carpeta.replace(":", "-");
    carpeta= carpeta.replace("*", "-");
    carpeta= carpeta.replace("\"", "-");
    carpeta= carpeta.replace("<", "-");
    carpeta= carpeta.replace(">", "-");
    carpeta= carpeta.replace("|", "-");

    return carpeta;
  }

  public List<String> crawl(String url){
    try
    {
      Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
      Document htmlDocument = connection.get();
      this.htmlDocument = htmlDocument;

      if(connection.response().statusCode() == 200)
      {
        System.out.println("\n**Visitando** sitio: " + url);
        Elements linksOnPage = htmlDocument.select("a[href]");

        for(Element link : linksOnPage)
        {

          this.links.add(link.absUrl("href"));
        }
      }
      if(!connection.response().contentType().contains("text/html"))
      {
        System.out.println("**Fallo** no es HTML");

      }

    }
    catch(IOException ioe)
    {
      System.out.println(ioe.getMessage());
    }
    return this.links;
  }

  public List<String> JustURL(List<String> links){

    List<String> csv = new ArrayList<>();

    for(String link : links)
    {
      if(link.endsWith(".csv"))
      {
        csv.add(link);
      }else
      if(link.endsWith("CSV")){
        csv.add(link);
      }
    }
    return csv;
  }

  public List<String> JustFiles(List<String> links){

    List<String> csv = new ArrayList<>();
    List<String> nombres = new ArrayList<>();
    String [] partes;

    for(String link : links)
    {
      if(link.endsWith(".csv"))
      {
        csv.add(link);
        partes=link.split("/");
        nombres.add(partes[partes.length-1]);
      }else if(link.endsWith("CSV")){
        csv.add(link);
        nombres.add("CSV_Archivo "+csv.size()+".csv");
      }
    }
    return nombres;
  }

  public boolean searchForCSV(String csv,String nombre, String url){
    boolean bool;
    File cPrincipal=new File("ARCHIVOS");
    cPrincipal.mkdir();

    File sitio=new File(cPrincipal+"/"+nombreCarpeta(url));
    sitio.mkdir();

    String folder = sitio.getPath()+"/CSV/";

    File dir = new File(folder);
    dir.mkdir();

    File file = new File(folder + nombre);


    try{
      URLConnection conn = new URL(csv).openConnection();
      conn.connect();
      System.out.println("\nempezando descarga: \n");
      System.out.println(">> URL: " + csv);
      System.out.println(">> Nombre: " + nombre);
      InputStream in = conn.getInputStream();
      OutputStream out = new FileOutputStream(file);

      int b = 0;
      char []ch;
      String normal;
      while (b != -1) {
        b = in.read();
        if (b != -1){
          ch = Character.toChars(b);
          normal=normalizar(""+ch[0]);
          out.write(normal.getBytes());
        }
      }

      out.close();
      in.close();

      CSV2XML xmlprueba=new CSV2XML();
      xmlprueba.convertFile(file.getPath(), file.getPath().replace(".csv",".xml" ), ",");
      RuteCSV = file.getPath();
      RuteXML = file.getPath().replace(".csv",".xml" );
      bool = true;
    }
    catch(IOException e) {
      e.printStackTrace();
      bool = false;
    }

    return bool;
  }

  public boolean searchForXML(List<String> links, String url){
    List<String> xml = new LinkedList<>();
    List<String> nombres = new LinkedList<>();
    String [] partes;
    String folder = "ARCHIVOS/"+nombreCarpeta(url)+"/XML/";
    System.out.println("Archivos XML");
    for(String link : links)
    {
      if(link.endsWith("ml"))
      {
        xml.add(link);
        System.out.println(link);
        partes=link.split("/");
        nombres.add(partes[partes.length-1]);
      }else if(link.endsWith("ML")){
        xml.add(link);
        System.out.println(link);
        nombres.add("XML_Archivo "+xml.size()+".xml");
      }

    }

    File dir = new File(folder);
    dir.mkdir();

    for (int i = 0; i < xml.size(); i++) {
      File file = new File(folder + nombres.get(i));
      try{
        URLConnection conn = new URL(xml.get(i)).openConnection();
        conn.connect();
        System.out.println("\nempezando descarga: \n");
        System.out.println(">> URL: " + xml.get(i));
        System.out.println(">> Nombre: " + nombres.get(i));
        InputStream in = conn.getInputStream();
        OutputStream out = new FileOutputStream(file);

        int b = 0;
        char []ch;
        String normal;
        while (b != -1) {
          b = in.read();
          if (b != -1){
            ch = Character.toChars(b);
            normal=normalizar(""+ch[0]);
            out.write(normal.getBytes());
          }
        }

        out.close();
        in.close();
      }
      catch(IOException e) {
        System.out.println(e.getMessage());
      }

    }

    return !xml.isEmpty();
  }

  /*
  public List<String> getLinks()
  {
  return this.links;
}
*/
public static String normalizar(String input) {
  String normalized = Normalizer.normalize(input, Normalizer.Form.NFD);
  Pattern pattern = Pattern.compile("\\P{ASCII}+");
  return pattern.matcher(normalized).replaceAll("");
}

public String getXML(){
  return RuteXML;
}

public String getCSV(){
  return RuteCSV;
}
}
