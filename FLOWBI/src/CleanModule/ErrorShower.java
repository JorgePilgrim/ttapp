/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package CleanModule;

import Windows.ColumnsName;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.xml.stream.XMLStreamException;

/**
*
* @author JorgeBaware
*/
public class ErrorShower {

  private List<List> vcor;
  private List<String> tiposCol1;
  private List<String> Nombres;
  private List<Boolean> verifier;
  Errores err;
  Archivo a;
  ColumnsName pane;

  public ErrorShower(){
    vcor = new ArrayList<>();
    tiposCol1 = new ArrayList<>();
    Nombres = new ArrayList<>();
    verifier = new ArrayList<>();
  }

  public void obErrores(String ruta) {
    err = new Errores(ruta);
    err.iniciar();
    err.analizar();

    vcor = err.getCorrecciones();
    tiposCol1 = err.getTiposCol();
    Nombres = err.getNombresCol();
    //System.out.println(tiposCol1);
    pane = new ColumnsName(Nombres,tiposCol1);
    JOptionPane.showMessageDialog(null,pane);
    verifier = pane.getValue();

    //System.out.println("\n\n\n"+err.getSb().toString());

  }

  public String corregir(String ruta) throws XMLStreamException, IOException {

    String route = "";
    //System.out.println("hola");

    a = new Archivo(ruta, vcor, tiposCol1,verifier);
    a.setErrxat(err.getErrorxat());
    System.out.println(a.getRuta());
    a.iniciar();
    a.preparar();
    a.corregir();
    route = a.NewCSV();



    return route;

  }

  public List<String> GetTypeColumn(){
    return tiposCol1;
  }
  public List<String> GetNameColumn(){

    return Nombres;
  }
}
