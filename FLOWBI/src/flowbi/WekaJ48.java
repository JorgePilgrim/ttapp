/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flowbi;

/**
 *
 * @author Edgar
 */    
import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import weka.classifiers.*;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;

public class WekaJ48 {
public static void main(String args[]) throws Exception {
     // train classifier
     J48 cls = new J48();
     File archivo = new File(WekaJ48.class.getResource("/flowbi/diabetes.arff.txt").getFile());
     Instances data = new Instances(new BufferedReader(new FileReader(archivo)));
     //Instances data = new Instances(new BufferedReader(new FileReader("C:\\Users\\Alejandro\\Desktop\\diabetes.arff.txt")));
     data.setClassIndex(data.numAttributes() - 1);
     cls.buildClassifier(data);

     // display classifier
     final javax.swing.JFrame jf = 
       new javax.swing.JFrame("Weka Classifier Tree Visualizer: J48");
     jf.setSize(500,400);
     jf.getContentPane().setLayout(new BorderLayout());
     TreeVisualizer tv = new TreeVisualizer(null,
         cls.graph(),
         new PlaceNode2());
     jf.getContentPane().add(tv, BorderLayout.CENTER);
     jf.addWindowListener(new java.awt.event.WindowAdapter() {
       public void windowClosing(java.awt.event.WindowEvent e) {
         jf.dispose();
       }
     });

     jf.setVisible(true);
     tv.fitToScreen();
   }

}
