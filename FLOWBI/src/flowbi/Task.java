/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
*
* @author JorgeBaware
*/
public class Task {
  private JLabel tarea; //la etiqueta para imprimir la imagen en el panel
  private String name; //el tipo de tarea al cual corresponde la accion(db/algoritmo/validacion/grafica)
  private String tareaid;
  private int movFlag; //bandera para detectar si es que se movio o no la tarea en el panel
  private int positionx; //posision x en el panel
  private int positiony; // posision y en el panel
  private int imageSize; // tamaño de la imagen en el panel
  private List<String> FPlines; // lineas en el que la tarea es el primer punto
  private List<String> SPlines; // lineas en el que la tarea es el segundo punto
  private JLabel Label;

  private JLabel addLabel(String name, int width,int height, int iconSize,MouseListener m1, MouseMotionListener m2){ // uso exclusivo de esta clase
    Label = new JLabel();// asignamos una nueva etiqueta
    Label.setIcon(ScaleImage(new ImageIcon(getClass().getResource("/Images/"+name+"F.png")),-1,iconSize)); // escalamos la imagen a utilizar
    Label.setPreferredSize(new java.awt.Dimension(iconSize, iconSize)); //seteamos el tamaño en la etiqueta
    Label.setBounds(width,height,iconSize,iconSize);
    //se agregan los listeners a las etiquetas.
    Label.addMouseListener(m1);
    Label.addMouseMotionListener(m2);

    return Label;
  }

  private ImageIcon ScaleImage(ImageIcon img, int an, int al){ // uso exclusivo de esta clase (resivimos la imagen, el ancho y el alto)
    ImageIcon nuevo = new ImageIcon(img.getImage().getScaledInstance(an,al,java.awt.Image.SCALE_DEFAULT)); //creamos una nueva IconImage con los nuevos valores de ancho y alto
    return nuevo;//retornamos el nuevo valor
  }

  public Task(String name, int bandera,int posx, int posy, int tam, MouseListener m1, MouseMotionListener m2){ //constructor
    //se inicializan variables
    this.name = name;
    this.movFlag = bandera;
    this.positionx = posx;
    this.positiony = posy;
    this.imageSize = tam;
    this.tarea = this.addLabel(this.name, positionx, positiony, imageSize, m1, m2);
    this.FPlines = new ArrayList<>();
    this.SPlines = new ArrayList<>();
  }

  public JLabel MoveWithMouse(){
    Point punto=MouseInfo.getPointerInfo().getLocation();//tomamos la posicion del mouse en un punto
    //se asignan a "x" y "y" los valores reales dentro del panel
    int x=punto.x-40-(imageSize*3)/2;
    int y=punto.y-85-(imageSize)/2;
    //se setea la nueva ubicacion y retorna el valor
    tarea.setBounds(x, y,imageSize,imageSize);
    return tarea;
  }

  public int WidthPosReset(int comp){
    //en esta funcion verificamos si ya se a movido la tarea en el panel si no se a movido cambiamos el estado de la bandera.
    if(movFlag == 0){
      movFlag =1;
      return comp-imageSize-8;
    }else{
      return comp;
    }
  }

  // Aqui empiezan los getters y Setters

  public String getTareaID(){
    return tareaid;
  }

  public String getName(){
    return name;
  }

  public JLabel getTask(){
    return tarea;
  }

  public int getMiddleX(){
    return (tarea.getX()+(tarea.getWidth()/2));
  }

  public int getMiddleY(){
    return (tarea.getY()+(tarea.getHeight()/2));
  }

  public List<String> getFPLines(){
    return FPlines;
  }

  public void FPAddLine(String line){
    FPlines.add(line);
  }

  public void FPDropLine(String line){
    FPlines.remove(line);
  }

  public boolean FPSearchLine(String line){
    return FPlines.contains(line);
  }

  public List<String> getSPLines(){
    return SPlines;
  }

  public void SPAddLine(String line){
    SPlines.add(line);
  }

  public void SPDropLine(String line){
    SPlines.remove(line);
  }

  public boolean SPSearchLine(String line){
    return SPlines.contains(line);
  }

  public void setTareaID(String id){
    tareaid = id;
    Label.setToolTipText(tareaid);
  }
}
