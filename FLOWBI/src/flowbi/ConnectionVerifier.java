/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;

import java.util.List;

/**
*
* @author Jorge
*/
public class ConnectionVerifier {
  public WorkFlow CreateWorkFlow(DataBaseTask db,Algorithm algorithm,int count){
    WorkFlow w = new WorkFlow("Workflow"+count);
    w.setDB(db);
    w.setAlgorithm(algorithm);

    return w;
  }
  public boolean VerifyConnectionDBtoAlgorithm(DataBaseTask db,Algorithm algorithm,List<Task> tasks){
    Task Tdb = null,Talg = null;
    boolean connection = false;
    for (Task t:tasks) {
      if (t.getTareaID() == db.getPadre()) {
        //System.out.println("soy la tarea:" + t.getTareaID());
        Tdb = t;
      }else if (t.getTareaID() == algorithm.getPadre()){
        //System.out.println("soy la tarea:" + t.getTareaID());
        Talg = t;
      }
    }

    for (String t:Tdb.getFPLines()) {
      if (Talg.SPSearchLine(t)) {
        connection = true;
        break;
      }
    }

    for (String t:Tdb.getSPLines()) {
      if (Talg.FPSearchLine(t)) {
        connection = true;
        break;
      }
    }
    return connection;
  }

  public String VerifyConnectionAlgorithmtoValidation(Algorithm algorithm,List<Task> tasks){
    Task Talg = null;
    String connection = "NA";

    for (Task t:tasks) {
      if (t.getTareaID() == algorithm.getPadre()){
        //System.out.println("soy la tarea:" + t.getTareaID());
        Talg = t;
      }
    }

    for (String l:Talg.getFPLines()) {

      for(Task t:tasks){

        if (t.getTareaID().contains("Validation")) {

          if(t.SPSearchLine(l)){

            connection = t.getTareaID();

          }

        }
      }
    }

    for (String l:Talg.getSPLines()) {

      for(Task t:tasks){

        if (t.getTareaID().contains("Validation")) {

          if(t.FPSearchLine(l)){

            connection = t.getTareaID();

          }

        }
      }
    }
    return connection;
  }

  public String VerifyConnectionAlgorithmtoGraphic(Algorithm algorithm,List<Task> tasks){
    Task Talg = null;
    String connection = "NA";

    for (Task t:tasks) {
      if (t.getTareaID() == algorithm.getPadre()){
        //System.out.println("soy la tarea:" + t.getTareaID());
        Talg = t;
      }
    }

    for (String l:Talg.getFPLines()) {

      for(Task t:tasks){

        if (t.getTareaID().contains("Graphic")) {

          if(t.SPSearchLine(l)){

            connection = t.getTareaID();

          }

        }
      }
    }

    for (String l:Talg.getSPLines()) {

      for(Task t:tasks){

        if (t.getTareaID().contains("Graphic")) {

          if(t.FPSearchLine(l)){

            connection = t.getTareaID();

          }

        }
      }
    }
    return connection;
  }
}
