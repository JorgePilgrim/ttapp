/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
*
* @author Jorge
*/
public class WorkFlow {

  private String name;
  private DataBaseTask DB;
  private Algorithm algoritmo;

  public WorkFlow(String name){
    this.name = name;
  }

  public void setDB(DataBaseTask tarea){
    DB = tarea;
  }
  public void setAlgorithm(Algorithm tarea){
    algoritmo = tarea;
  }
  public DataBaseTask getDB(){
    return DB;
  }
  public Algorithm gtAlgorithm(){
    return algoritmo;
  }

  public void Play(){
    try {
      System.out.println(DB.getPadre()+"->"+algoritmo.getPadre()+"("+
      algoritmo.getName()+")->"+algoritmo.getValidation()+"->"+algoritmo.getGraphic());
      algoritmo.setDataSet(DB.getCSV());
      algoritmo.PlayAlgorithm();
    } catch (Exception ex) {
      Logger.getLogger(WorkFlow.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}
