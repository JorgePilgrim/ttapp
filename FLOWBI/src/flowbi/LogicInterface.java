/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
*
* @author Jorge
*/
public class LogicInterface {

  public ImageIcon ScaleImage(ImageIcon img, int an, int al){
    //se escala una imagen
    ImageIcon nuevo = new ImageIcon(img.getImage().getScaledInstance(an,al,java.awt.Image.SCALE_DEFAULT));
    return nuevo;
  }

  public String ConfigDB(String tareaDB){

    String Respuesta = "NA";
    JFileChooser selectorFile=new JFileChooser();
    FileNameExtensionFilter filtro = new FileNameExtensionFilter("CSV","csv");
    selectorFile.setFileFilter(filtro);
    File t = null;

    selectorFile.setDialogTitle("Choose a CSV File to "+tareaDB);
    int flag=selectorFile.showOpenDialog(null);

    if (flag==JFileChooser.APPROVE_OPTION){
      try {
        t = selectorFile.getSelectedFile();
        if (t!=null){
        }else{
          JOptionPane.showConfirmDialog(null,
          "No se pudo cargar el archivo", "Configuration "+tareaDB, JOptionPane.DEFAULT_OPTION);
        }
      } catch (Exception e) {
        JOptionPane.showConfirmDialog(null,
        "No se pudo cargar el archivo", "Configuration "+tareaDB, JOptionPane.DEFAULT_OPTION);
      }
    }
    if(t != null)
    {
      Respuesta = t.getPath();
    }

    return Respuesta;
  }

  public String ConfigAlgorithm(String name){
    String ret = "NA";
    Object[] options = {"Apriori","K-nn"};
    Object value = JOptionPane.showInputDialog(null,
    "Seleccione su Algoritmo a ultilizar",
    "Algoritmo para tarea "+ name,
    JOptionPane.QUESTION_MESSAGE,
    null,
    options,
    options[0]);
    if(value != null){
      ret = value.toString();
    }
    return ret;
  }

  public String ConfigKnnTraining(String tareaAL){
    String Respuesta = "NA";
    JFileChooser selectorFile=new JFileChooser();
    FileNameExtensionFilter filtro = new FileNameExtensionFilter("CSV","csv");
    selectorFile.setFileFilter(filtro);
    File t = null;

    selectorFile.setDialogTitle("Choose a CSV File to "+tareaAL);
    int flag=selectorFile.showOpenDialog(null);

    if (flag==JFileChooser.APPROVE_OPTION){
      try {
        t = selectorFile.getSelectedFile();
        if (t!=null){
        }else{
          JOptionPane.showConfirmDialog(null,
          "No se pudo cargar el archivo", "Configuration "+tareaAL, JOptionPane.DEFAULT_OPTION);
        }
      } catch (Exception e) {
        JOptionPane.showConfirmDialog(null,
        "No se pudo cargar el archivo", "Configuration "+tareaAL, JOptionPane.DEFAULT_OPTION);
      }
    }
    if(t != null)
    {
      Respuesta = t.getPath();
    }

    return Respuesta;
  }

  public List<WorkFlow> CreateWorkFlows(DataBaseTask db,List<Algorithm> algorithms,List<Task> tasks,int Counter){
    ConnectionVerifier cv = new ConnectionVerifier();
    List<WorkFlow> wf  = new ArrayList<>();
    boolean verifier = false;
    int count = 0;
    for(Algorithm t:algorithms){

      verifier = false;

      if(cv.VerifyConnectionDBtoAlgorithm(db, t, tasks)){

        t.setValidation(cv.VerifyConnectionAlgorithmtoValidation(t, tasks));
        t.setGraphic(cv.VerifyConnectionAlgorithmtoGraphic(t, tasks));
        for (WorkFlow vrf:wf) {

          if (vrf.gtAlgorithm().getPadre() == t.getPadre()) {

            verifier = true;
          }
        }

        if (!verifier) {

          wf.add(cv.CreateWorkFlow(db,t,count));
          count++;
        }
      }
    }
    return wf;
  }

  public String Recomendation(List<String> columnTypes){
    String recomendation = "";
    int contKnn = 0;
    int contApriori = 0;

    for (String c:columnTypes) {
      if (c.contains("fecha") || c.contains("varchar")){
        contApriori++;
      }
      if(c.contains("numero") || c.contains("coordenadas")){
        contKnn++;
      }
    }

    if(contApriori > contKnn){
      JOptionPane.showConfirmDialog(null,
      "Debido a los tipos de datos del archivo se "
      + "recomienda usar Apriori",
      "Recomendacion", JOptionPane.DEFAULT_OPTION);
    }else{
      JOptionPane.showConfirmDialog(null,
      "Debido a los tipos de datos del archivo se "
      + "recomienda usar Knn",
      "Recomendacion", JOptionPane.DEFAULT_OPTION);
    }

    return recomendation;
  }
}
