/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
*
* @author Jorge
*/
public class ActionListeners {

  //esta clase solo se usa para hacer operaciones dentro de algun actionListener

  public int DraggedActionDone(List<Task> tasks, List<Line> lines, MouseEvent me, int widthPos){
    //este metodo se usa para cambiar la posicion de las clases en el panel de la interfaz de workspace

    for(Task labelComp:tasks) {//este forEach recorre todas las tareas que tenemos listadas

      if(me.getSource() == labelComp.getTask()){//compara hasta encontrar la etiqueta en la que se dio click

        for(Line test:lines){//este forEach recorre todas las lineas que tenemos listadas

          if(labelComp.FPSearchLine(test.getName())){//comparamos si la linea existe como primer punto en nuestra tarea.

            test.SetFirstCoord(labelComp.getMiddleX(),labelComp.getMiddleY());//si existe cambiamos la coordenada de la linea.

          }else
          if(labelComp.SPSearchLine(test.getName())){//si no existe, buscamos su existencia como segundo punto en nuestra tarea.

            test.SetSecondCoord(labelComp.getMiddleX(),labelComp.getMiddleY());//si existe cambiamos la coordenada de la linea

          }
        }//terminan las comparaciones de lineas

        labelComp.MoveWithMouse();//se llama a una funcion dentro de la tarea que mueve la posicion de esta dentro del panel

        widthPos=labelComp.WidthPosReset(widthPos);//de ser necesario se cambia el valor de la posicion inicial de las tareas futuras a crear
      }
    }
    return widthPos;//regresamos la posicion.
  }

  public Task TaskSelected(List<Task> tasks,MouseEvent me){
    //obtenemos la tarea seleccionada

    Task Selected = null;

    for(Task labelComp:tasks){//recorremos la lista de nuestras tareas
      if(me.getSource() == labelComp.getTask()){//si coincide con la tarea seleccionada se asigna a una variable dicha tarea reconocida
        Selected = labelComp;
      }
    }
    return Selected;
  }

  public JLabel EraseSelected(List<Task> tasks,List<Line> lines, Task labelErased, Graphics2D g2d){
    //se encarga de borrar la tarea seleccionada

    int ind = 0;
    JLabel returned = null;

    for(Task labelComp:tasks){//recorremos todas las tareas seleccionadas.
      if(labelErased == labelComp){//si se encuentra la tarea selecionada en la lista
        this.EraseLines(labelComp.getFPLines(), lines, tasks, labelComp, g2d);//borramos de la lista 1 todas las lineas relacionadas con la tarea
        this.EraseLines(labelComp.getSPLines(), lines, tasks, labelComp, g2d);//borramos de la lista 2 todas las lineas relacionadas con la tarea
        returned = labelComp.getTask();//asignamos la etiqueta que se borrara
        tasks.remove(ind);//borramos de la lista de tareas dicha etiqueta
        break;//se deja de buscar en la lista
      }
      ind++;
    }
    return returned;
  }

  public void EraseLines(List<String> Dummy, List<Line> lines, List<Task> tasks, Task comp, Graphics2D g2d){
    //borramos lineas de una lista
    if(Dummy != null){ // si la lista no esta vacia entra a las siguientes acciones
      for(String linea:Dummy){//recorremos toda la lista que se envia de la tarea
        for(Line line:lines){//recorremos toda la lista de lineas que tenemos en el panel
          if(line.getName() == linea){//cuando estas coinciden
            line.SetFirstCoord(-5, -5);//seteamos fuera del panel las lineas
            line.SetSecondCoord(-5, -5);//seteamos fuera del panel las lineas
          }
        }
        lines.remove(linea);//se remueven las lineas de la lista general de lineas

        for(Task task:tasks){//recorremos toda la lista de tareas general para borrar tambien la linea de las tareas ajenas

          if(task != comp){//si la tarea no es la tarea a borrar se hace lo siguiente

            if(task.FPSearchLine(linea)){//buscamos la existencia de la linea como primer punto en las otras tareas

              task.FPDropLine(linea);//se elimina las lineas
            }
            else{
              if(task.SPSearchLine(linea)){//buscamos la existencia de la linea como segundo punto en las otras tareas

                task.SPDropLine(linea);//se elimina las lineas

              }

            }

          }

        }
      }
    }
  }

  public void paintLines(List<Line> lines, Graphics2D g2d){
    //dibujamos todas las lineas de la lista en el panel mientras la lista no este vacia.

    if(lines != null){

      for(Line test:lines){

        g2d.setColor(Color.BLACK);
        g2d.setStroke(new BasicStroke((float)5.0));
        test.DrawALine(g2d);

      }
    }
  }

  public int VisualConection(List<Task> l,Task Selected, Task tarea, List<Line> lines, int lineCounter){
    //se encarga de la coneccion de las tareas

    if(Selected != null && tarea != null){// si las tareas conectadas no estan vacias.

      if(Selected != tarea){// y las tareas son diferentes entre ellas.

        if(this.TaskValidation(Selected.getName(), tarea.getName())){//si pasa la validacion

          if (this.ExistValidation(l, Selected, tarea)) {//verifica que no se pueda conectar mas tareas a un algoritmo

            String lineName = "linea"+lineCounter;//se asigna el nombre id de la linea a una variable
            Selected.FPAddLine(lineName);//se agrega como primer punto a la tarea seleccionada primero
            tarea.SPAddLine(lineName);//se agrega como segundo punto a la tarea seleccionada despues
            lines.add(new Line(Selected.getMiddleX(),Selected.getMiddleY(),
            tarea.getMiddleX(),tarea.getMiddleY(),lineName));//se agrega una nueva linea a la lista de lineas generales
            lineCounter++;// aumenta el contador

          }
          else{
            JOptionPane.showConfirmDialog(null,
            "No puedes  Asignar esta tarea ",
            "Conexion "+Selected.getTareaID()+" con "+tarea.getTareaID(), JOptionPane.DEFAULT_OPTION);
          }

        }
        else{//no pasa la prueba de validacion
          JOptionPane.showConfirmDialog(null,
          "No son compatibles estas tareas",
          "Conexion "+Selected.getTareaID()+" con "+tarea.getTareaID(), JOptionPane.DEFAULT_OPTION);

        }
      }
      else{// si las tareas son la misma
        JOptionPane.showConfirmDialog(null,
        "no debes unir la misma tarea",
        "Conexion "+Selected.getTareaID()+" con "+tarea.getTareaID(),JOptionPane.DEFAULT_OPTION);
      }
    }
    else{// si una etiqueta selecionada es vacia
      JOptionPane.showConfirmDialog(null,
      "debes seleccionar tareas a unir",
      "Error de conexion",JOptionPane.DEFAULT_OPTION);
    }
    return lineCounter; //se regresa un contador.
  }

  private boolean ExistValidation(List<Task> tareas,Task tarea1, Task tarea2){
    Task main = null;//tarea principal
    Task Slave = null;//tarea secundaria
    boolean Ans = true;//booleano de respuesta
    boolean vrf = true;

    if (tarea1.getName().contains("Algorithm")) {
      //si la tarea1 es el algoritmo se asigna a main y la segunda tarea como esclavo

      main = tarea1;
      Slave = tarea2;

    }else if (tarea2.getName().contains("Algorithm")){
      //si la tarea2 es el algoritmo se asigna a main y la primera tarea como esclavo

      main = tarea2;
      Slave = tarea1;

    }
    if (main.getName().contains("Algorithm")) {//si alguna de las dos tareas que se envian es un algoritmo entra a esta parte

      if (Slave.getName().contains("Validation") || Slave.getName().contains("Graphic")) {//si la tarea esclavo es "DB" entonces no hace nada

      for (String l:main.getFPLines()) {//recorremos todas las lineas que tenemos en la tarea

        for (Task t:tareas) { // recorremos las tareas existentes

          if(t != main){//no realiza la accion en la tarea que es igual

            if (t.SPSearchLine(l)) { // si la tarea (punto 2) esta conectada a main (punto 1) entra

              if (t.getName().contains(Slave.getName())) { //si la tarea encontrada es del mismo tipo que la que se quiere conectar retornamos un false

                Ans = false;
              }
            }
          }
        }
      }

      for (String l:main.getSPLines()) {//recorremos todas las lineas que tenemos en la tarea

        for (Task t:tareas) { // recorremos las tareas existentes

          if(t != main){//no realiza la accion en la tarea que es igual

            if (t.FPSearchLine(l)) {// si la tarea (punto 1) esta conectada a main (punto 2) entra

              if (t.getName().contains(Slave.getName())) {//si la tarea encontrada es del mismo tipo que la que se quiere conectar retornamos un false

                Ans = false;
              }
            }
          }
        }
      }

      for (String l:Slave.getFPLines()) {//recorremos todas las lineas que tenemos en la tarea

        for (Task t:tareas) { // recorremos las tareas existentes

          if(t != Slave){//no realiza la accion en la tarea que es igual

            if (t.SPSearchLine(l)) { // si la tarea (punto 1) esta conectada a main (punto 2) entra

              if (t.getName().contains(main.getName())) { //si la tarea encontrada es del mismo tipo que la que se quiere conectar retornamos un false

                Ans = false;
              }
            }
          }
        }
      }

      for (String l:Slave.getSPLines()) {//recorremos todas las lineas que tenemos en la tarea

        for (Task t:tareas) { // recorremos las tareas existentes

          if(t != Slave){//no realiza la accion en la tarea que es igual

            if (t.FPSearchLine(l)) { // si la tarea (punto 1) esta conectada a main (punto 2) entra

              if (t.getName().contains(main.getName())) { //si la tarea encontrada es del mismo tipo que la que se quiere conectar retornamos un false

                Ans = false;
              }
            }
          }
        }
      }
    }
    else{
      Ans = true;
    }
  } else{
    Ans = true;
  }

  return Ans;
}

private boolean TaskValidation(String name1, String name2){
  //se regresan los valores de validacion.
  boolean val = false;
  if((name1 == "DB" && name2 == "Algorithm") ||
  (name1 == "Algorithm" && (name2 == "Validation" || name2 == "DB" || name2 == "Graphic")) ||
  (name1 == "Validation" && (name2 == "Algorithm")) ||
  (name1 == "Graphic" && (name2 == "Algorithm"))){
    val = true;
  }else{
    val = false;
  }
  return val;
}


}
