/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;

import CleanModule.CSV2XML;
import CleanModule.ErrorShower;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.stream.XMLStreamException;

/**
*
* @author Jorge
*/
public class DataBaseTask {

  private String ClearCSV;
  private String padre;
  Files handler;
  LogicInterface LI;

  public DataBaseTask(String route, String dad) throws XMLStreamException, IOException{

    handler = new Files();
    LI = new LogicInterface();
    ClearCSV = handler.TransformCSVToCSVClean(route);
    LI.Recomendation(handler.getColumnsType());
    padre = dad;

  }

  public DataBaseTask(String dad){

    padre = dad;
  }



  public void setRoute(String c){
    try {
      ClearCSV = handler.TransformCSVToCSVClean(c);
      LI.Recomendation(handler.getColumnsType());
    } catch (XMLStreamException ex) {
      Logger.getLogger(DataBaseTask.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(DataBaseTask.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public void setPadre(String c){
    padre = c;
  }

  public String getCSV(){
    return ClearCSV;
  }

  public String getPadre(){
    return padre;
  }
}
