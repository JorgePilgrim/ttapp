/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;

import java.io.IOException;
import javax.xml.stream.XMLStreamException;

/**
*
* @author Jorge
*/
public class Algorithm {

  private String fileRoute; //ruta del dataset
  private String Name;
  private String Padre;
  private String validationID;
  private String graphicID;
  private AprioriAlgorithm AA;
  private KnnAlgorithm KA;

  Files handler;

  public Algorithm(String name, String padre){
    //constructor
    Name = name;
    Padre = padre;
    AA = null;
    KA = null;
    validationID = "NA";
    graphicID = "NA";
    handler = new Files();
    this.SetAlgorithm();
  }

  public Algorithm(String name, String padre, String training) throws XMLStreamException, IOException{
    //constructor
    Name = name;
    Padre = padre;
    AA = null;
    validationID = "NA";
    graphicID = "NA";
    handler = new Files();
    this.SetAlgorithm(training);
  }

  private void SetAlgorithm(){

    if(Name.contains("Apriori")){
      AA = new AprioriAlgorithm();
      KA = null;
    }

  }

  private void SetAlgorithm(String training) throws XMLStreamException, IOException{

    if(Name.contains("K-nn")){
      KA = new KnnAlgorithm(this.setTrainingSet(training));
      AA = null;
    }

  }

  public void PlayAlgorithm() throws Exception{
    if(Name.contains("Apriori")){
      AA.SetFile(fileRoute);
      AA.algorithmApriori();
    }else{
      KA.SetFile(fileRoute);
      KA.playKnn();
        if (validationID != "NA") {
            KA.validateKnn();
        }
    }
  }

  public String setTrainingSet(String url) throws XMLStreamException, IOException{
    return handler.TransformCSVToCSVClean(url);
  }

  public void setDataSet(String url){
    fileRoute = url;
  }

  public void setName(String N){
    Name = N;
    this.SetAlgorithm();
  }

  public void setName(String N, String file) throws XMLStreamException, IOException{
    Name = N;
    this.SetAlgorithm(file);
  }

  public void setPadre(String f){
    Padre = f;
  }

  public void setValidation(String val){
    validationID = val;
  }

  public void setGraphic(String val){
    graphicID = val;
  }

  public String getDataSet(){
    return fileRoute;
  }

  public String getName(){
    return Name;
  }

  public String getPadre(){
    return Padre;
  }

  public String getValidation(){
    return validationID;
  }

  public String getGraphic(){
    return graphicID;
  }

}
