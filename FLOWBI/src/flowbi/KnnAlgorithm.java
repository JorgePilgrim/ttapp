/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;



import Windows.ColumnsName;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import weka.classifiers.lazy.IBk;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.unsupervised.attribute.ReplaceMissingValues;

/**
*
* @author JorgeBaware
*/
public class KnnAlgorithm {
  private String Training;
  private String fileRoute;
  private int k;
  private Files f;
  private IBk Knn;
  Instances train;
  Instances dataTst;

  public KnnAlgorithm(String trained){
    try {
      f = new Files();
      Training = trained;
      prepareKnn(this.SetK());

    } catch (Exception ex) {
      Logger.getLogger(KnnAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private int SetK(){
    boolean bandera;
    int numero = 0;
    do{
      try{
        numero=Integer.parseInt(javax.swing.JOptionPane.showInputDialog("introduce un valor para 'k'"));
        bandera=true;
      }
      catch(NumberFormatException e){
        javax.swing.JOptionPane.showMessageDialog(null, "Solo se permiten valores numericos");
        bandera=false;
      }
    }while(!bandera);

    return numero;
  }

  public void SetFile(String Archivo){
    fileRoute = Archivo;
  }

  private void prepareKnn(int ke) throws Exception{
    k = ke;
    train = f.fileReader(Training);
    List<String> name = new ArrayList<>();
    List<String> tipos = new ArrayList<>();
    boolean mainclass = false;
    List<Boolean> select = new ArrayList<>();
    int classSelected = 0;
    int cont;

    for (int i = 0; i < train.numAttributes(); i++) {
      Attribute at = train.instance(0).attribute(i);
      name.add(at.name());
      if (at.isDate()) {
        tipos.add("fecha");
      }else if (at.isNominal()) {
        tipos.add("Nominal");
      }else if (at.isNumeric()) {
        tipos.add("numero");
      }else if (at.isString()) {
        tipos.add("varchar");
      }else{
        tipos.add("-");
      }
    }
    ColumnsName pane = new ColumnsName(name,tipos,"Selecciona la columna que se usara como clase");
    do{
      cont = 0;
      JOptionPane.showMessageDialog(null,pane);
      select = pane.getValue();
      for (int i = 0; i < select.size(); i++) {
        if (select.get(i)) {
          classSelected = i;
          cont++;
        }
      }
      if (cont == 1) {
        mainclass = true;
      }else{
        JOptionPane.showMessageDialog(null,"Selecciona 1 columna como clase");
      }
    }while(!mainclass);


    train.setClassIndex(classSelected);
    Knn = new IBk();
    Knn.setKNN(k);
    Knn.setCrossValidate(true);
    Knn.setWindowSize(0);
    Knn.setMeanSquared(false);
    //System.out.println(train);
  }

  public void playKnn(){
    try {
      dataTst = f.fileReader(fileRoute);
      if (train.checkInstance(dataTst.firstInstance())) {
        
        Knn.buildClassifier(train);
          
        dataTst.setClassIndex(dataTst.numAttributes() - 1);

        int ClassCol = dataTst.numAttributes() - 1;

        List<String> val = new ArrayList<>();

        Enumeration <Object> obj = train.attribute(ClassCol).enumerateValues();
        while(obj.hasMoreElements()){
          Object ob = obj.nextElement();
          val.add(ob.toString());
        }


        for (int i = 0; i < dataTst.numInstances(); i++) {

          int cl = (int)Knn.classifyInstance(dataTst.instance(i));
          System.out.println(val.get(cl));

        }
      }else{
        JOptionPane.showMessageDialog(null, "el dataset de entrenamiento no es compatible con el de prueba");
      }




    } catch (Exception ex) {
      Logger.getLogger(KnnAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  public void validateKnn() throws Exception{
      String val;
      /*Knn.setKNN(k);
      System.out.println(k);
      Knn.setCrossValidate(true);*/
      Knn.buildClassifier(train);
      val = Knn.toString();
      System.out.println(val);
      Knn.setCrossValidate(false);
      System.out.println(Knn.getKNN());
      
      
  }
}
