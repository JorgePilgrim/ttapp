/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;

import java.awt.Graphics2D;

/**
*
* @author Jorge
*/
public class Line {
  private int x1;//posicion x del primer punto.
  private int y1;//posicion y del primer punto.
  private int x2;//posicion x del segundo punto.
  private int y2;//posicion y del segundo punto.
  private String name;// nombre identificador.

  public Line(int x1, int y1, int x2, int y2, String name){
    //se asignan las variables.
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    this.name = name;
  }

  public void DrawALine(Graphics2D g2d){
    //dibujamos la linea
    g2d.drawLine(x1, y1, x2, y2);
  }
  //aqui empiezan los getters y setters
  public int getX1(){
    return x1;
  }

  public int getY1(){
    return y1;
  }

  public int getX2(){
    return x2;
  }

  public int getY2(){
    return y2;
  }

  public String getName(){
    return name;
  }

  public void SetFirstCoord(int x1, int y1){
    // se setea las nuevas coordenadas del primer punto
    this.x1 = x1;
    this.y1 = y1;
  }

  public void SetSecondCoord(int x2, int y2){
    //se setea las nuevas coordenadas del segundo punto
    this.x2 = x2;
    this.y2 = y2;
  }
}
