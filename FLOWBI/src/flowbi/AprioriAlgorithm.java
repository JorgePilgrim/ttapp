/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;

import weka.associations.Apriori;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Discretize;


/**
*
* @author Jorge
*/
public class AprioriAlgorithm {
  String fileRoute = "";
  Files f;
  public AprioriAlgorithm(){
    f = new Files();
  }

  public void SetFile(String Archivo){
    fileRoute = Archivo;
  }

  public Apriori algorithmApriori() throws Exception{

    Instances data = f.fileReader(fileRoute);//se lee la ruta del archivo.

    data = f.DiscretizeData(data);

    Apriori algorithm = new Apriori(); // se crea un objeto algoritmo.

    algorithm.setMinMetric(0.5);//se setea la confianza
    //algorithm.setLowerBoundMinSupport(0.215);
    algorithm.setNumRules(5);// se setea el numero de reglas

    if(data != null){//si los datos no son nulos crea las asociasiones
      algorithm.buildAssociations(data);
    }
    else{
      System.out.println("Error");
    }
    System.out.println(algorithm.toString());

    return algorithm;//regresa el algoritmo
  }

}
