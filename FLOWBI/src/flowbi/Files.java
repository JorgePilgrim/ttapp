/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package flowbi;

import CleanModule.CSV2XML;
import CleanModule.ErrorShower;
import Windows.ColumnsName;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.stream.XMLStreamException;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Discretize;
import weka.filters.unsupervised.attribute.Remove;

/**
*
* @author JorgeBaware
*/
public class Files {

  List<String> typeColumns;
  ColumnsName pane;

  public Files(){
    typeColumns = new ArrayList<>();
  }

  public String TransformCSVToCSVClean(String ruta) throws XMLStreamException, IOException{
    ErrorShower error = new ErrorShower();
    //File file = new File(ruta);
    String newRuta;
    String ultimateRoute="";

    if (ruta.contains("csv")) {
      newRuta = ruta.replace(".csv",".xml");
    }else{
      newRuta = ruta.replace(".CSV",".xml");
    }
    if (!newRuta.contains(".xml")) {
      JOptionPane.showConfirmDialog(null,
      "Archivo No soportado", "Ok!", JOptionPane.DEFAULT_OPTION);
    }else{

      CSV2XML xmlprueba=new CSV2XML();
      xmlprueba.convertFile(ruta,newRuta, ",");
      error.obErrores(newRuta);
      typeColumns = error.GetTypeColumn();
      ultimateRoute = error.corregir(newRuta);

    }
    return ultimateRoute;
  }

  public Instances fileReader(String arffInput){

    ConverterUtils.DataSource source = null;
    Instances data = null;

    try {
      source = new ConverterUtils.DataSource(arffInput);
      data = source.getDataSet();
    } catch (Exception ex) {
      System.out.println("ERRROR Loading file");
    }

    return data;
  }

  public Instances FilterAttributes(Instances data, String list){
    Instances t = data;
    try {
      String[] opts = new String[]{"-R",list};
      Remove remove = new Remove();
      remove.setOptions(opts);
      remove.setInputFormat(data);
      t = Filter.useFilter(data, remove);

    } catch (Exception ex) {
      Logger.getLogger(Files.class.getName()).log(Level.SEVERE, null, ex);
    }
    return t;
  }

  public Instances DiscretizeData(Instances data){

    try {
      Discretize dis = new Discretize();
      dis.setInputFormat(data);
      data = Filter.useFilter(data, dis);
    } catch (Exception ex) {
      Logger.getLogger(Files.class.getName()).log(Level.SEVERE, null, ex);
    }

    return data;
  }

  public List<String> getColumnsType(){
    return typeColumns;
  }
}
