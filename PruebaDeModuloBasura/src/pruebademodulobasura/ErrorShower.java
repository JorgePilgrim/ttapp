/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebademodulobasura;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author JorgeBaware
 */
public class ErrorShower {
    
    private List<List> vcor;
    private List<String> tiposCol1;
    Errores err;
    Archivo a;
    
    public ErrorShower(){
        vcor = new ArrayList<>();
        tiposCol1 = new ArrayList<>();
    }
    
    public void obErrores(String ruta) {
        err = new Errores(ruta);
        err.iniciar();
        err.analizar();
        
        vcor = err.getCorrecciones();
        tiposCol1 = err.getTiposCol();
        
        /*for (int i = 0; i < tiposCol1.size(); i++) {
            System.out.println(tiposCol1.get(i));
        }*/
        
        //System.out.println("\n\n\n"+err.getSb().toString());
        
    }
    
    public void corregir(String ruta) throws XMLStreamException, IOException {

        
            //System.out.println("hola");
                
                a = new Archivo(ruta, vcor, tiposCol1);
                a.setErrxat(err.getErrorxat());
                System.out.println(a.getRuta());
                a.iniciar();
                a.preparar();
                a.corregir();
                a.NewCSV();
                /*
                System.out.println("Archivo preparado");
                

        
        */
    }

   public List<String> GetTypeColumn(){
       return tiposCol1;
   }
}
