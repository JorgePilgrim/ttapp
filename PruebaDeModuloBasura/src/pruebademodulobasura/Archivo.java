
package pruebademodulobasura;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.jespxml.JespXML;
import org.jespxml.modelo.Tag;

/**
 *
 * @author ALAN
 */
public class Archivo {
    
    private String ruta;
    private List<List> listacor;
    
    
    //COLUMNAS/ATRIBUTOS
    private List<String> tiposCol;
    private List<String> errxat;
    private long totalerr;
    private int numCol; //LISTO
    private List<Atributo> columnas;
    
    //FILAS/REIGISTROS
    private long tamFilas;//LISTO
    List<Tag> filas;//LISTO
    
    //OTROS
    private Tag raiz;
    String valorHijo;
    String NewFile = "";
    List<Tag> etqHijos;
    
    
    public Archivo(String ruta,List<List> listacor,List<String> tiposCol){
        this.ruta= ruta;
        this.tiposCol= tiposCol;
        this.listacor=listacor;
        columnas=new ArrayList<>();
    }

    public int getNumCol() {
        return numCol;
    }

    public long getTamFilas() {
        return tamFilas;
    }

    public String getRuta() {
        return ruta;
    }
    
    public List<Atributo> getColumnas() {
        return columnas;
    }
    
     public void setErrxat(List<String> errxat) {
        this.errxat = errxat;
    }
    
    

    public void iniciar(){
        try {

            JespXML xmlDoc = new JespXML(new File(ruta.trim()));

            raiz = xmlDoc.leerXML();

            filas=new ArrayList(raiz.getTagsHijos());

            tamFilas=filas.size();

            numCol=filas.get(3).getTagsHijos().size();


        } catch (Exception e) {
            System.out.println("Error al iniciar");
        }
    }
    
    public void preparar(){
        
        int listafilastam =listacor.get(1).size();

        for (int j = 0; j<numCol;j++) {
            System.out.println("Preparando columna "+j);
            //VAR PROM
            double promD =0;

            String moda="";

            //VAR MAX
            double valMax=0;

            //VAR MIN
            double valMin=0;
            
            double aux;
            
            int ind1=0;
            
            boolean init = true;
            ArrayList<Integer> rowError = new ArrayList<>();
            
            Atributo c = new Atributo();
            c.setNum(j);
            c.setNombre(filas.get(j).getTagsHijos().get(j).getNombre());
            c.setTipo(tiposCol.get(j));
            
            ArrayList <Integer> rep = new ArrayList<>();
            ArrayList <String> etq = new ArrayList<>();
            
            if(listafilastam > 0){
                
                int value = 0;
                int conter=0;
                
                c.setErr(errxat.get(j));
                for (Object t:listacor.get(2)) {
                    ind1 = Integer.parseInt(t.toString());
                    if (ind1 == j) {
                        value = Integer.parseInt(listacor.get(1).get(conter).toString());
                        rowError.add(value);
                    //System.out.print(ind1);
                    }
                    conter++;
                }
            }
            
           
            
            for (int i = 0; i < tamFilas; i++) {
                
                etqHijos=new ArrayList(filas.get(i).getTagsHijos());
                valorHijo = etqHijos.get(c.getNum()).getContenido();
                
                 if (!rowError.contains(i)) {
                     if(etq.contains(valorHijo.toString())){
                         int cont=0;
                         for (String t:etq) {
                             if (t.contains(valorHijo.toString())) {
                                 int co = rep.get(cont)+1;
                                 rep.set(cont, co);
                             }
                             cont++;
                         }
                     }else{
                         etq.add(valorHijo.toString());
                         rep.add(1);
                     }
                 }

                 if(c.getTipo().contains("numero")){
                     
                     if (!rowError.contains(i)) {
                         /////////////////////////Promedio
                         promD+=Double.parseDouble(valorHijo);
                         aux=Double.parseDouble(valorHijo);
                         
                         if(init){
                             valMax=aux;
                             valMin=aux;
                             init = false;
                         }
                         else{
                             if(valMax<aux){
                                 valMax=aux;
                             }
                             if(valMin>aux){
                                 valMin=aux;
                             }
                         }
                     }
                 }
                /////////////////////////////////////////////////////////////////////////////////////////////////////
            }
            
            int indModa = 0;
            int repModa = 0;
            int contModa = 0;
            
            
            
            for (int t:rep) {
                if(t > repModa){
                    repModa = t;
                    indModa = contModa;
                }
                contModa++;
            }
            moda = etq.get(indModa);
                
            if(c.getTipo().contains("numero")){
                promD=promD/(tamFilas-rowError.size());
                
                if(c.getTipo().contains("flotante")){
                    c.setMax(valMax+"");
                    c.setMin(valMin+"");
                    c.setPromedio(promD+"");
                }else{
                    c.setMax(((long)valMax)+"");
                    c.setMin(((long)valMin)+"");
                    c.setPromedio(((long)promD)+"");
                }
                c.setModa(moda);
            }else{
                c.setMax("Valor no numerico");
                c.setMin("Valor no numerico");
                c.setPromedio("Valor no numerico");
                c.setModa(moda);
            }
            
            columnas.add(c);
            //System.out.println(c.moda);
            
        }
    }
    
    public void corregir() throws XMLStreamException, IOException{
        
        
        //System.out.println(listacor.get(2));
        //System.out.println(listacor.get(3));
        int i = 0;
        int x = 0;

                
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = null;
        xtw = xof.createXMLStreamWriter(new FileWriter(this.ruta));
        
        xtw.writeStartDocument("utf-8", "1.0");
        xtw.writeCharacters("\n");
        xtw.writeStartElement("Tabla");
        for(x = 0; x <tamFilas; x++){
            
            int ind1=0;
            int contador = 0;
            int ind2 = 0;
            ArrayList<Integer> errors = new ArrayList<>();
            xtw.writeCharacters("\n");
            xtw.writeStartElement("row");
            
            
            for (Object tst:listacor.get(1)) {
                
                    ind1 = Integer.parseInt(tst.toString());
                    if (ind1 == x) {
                        ind2 = Integer.parseInt(listacor.get(2).get(contador).toString());
                        
                        errors.add(ind2);
                    } 
                    contador++;
                }
            //System.out.println(errors);
                //System.out.print(errors);
            for (i = 0; i < numCol; ) {
                
                
                etqHijos=new ArrayList(filas.get(x).getTagsHijos());
                valorHijo = etqHijos.get(columnas.get(i).getNum()).getContenido();
                //System.out.print(columnas.get(i).getPromedio());
                
                if(x == 0 && i == 0){
                    for (int cont = 0; cont < numCol;) {
                       NewFile += etqHijos.get(cont).getNombre();
                       cont++;
                       if(cont == numCol){
                           NewFile += "\n";
                       }else{
                           NewFile += ",";
                       }
                    }
                    
                }
                
                xtw.writeCharacters("\n");
                
                xtw.writeStartElement(etqHijos.get(columnas.get(i).getNum()).getNombre());
                //System.out.print(tiposCol.get(i));
                if(errors.contains(i)){
                    //System.out.print("* Aqui Error *");
                    if(tiposCol.get(i).toString().contains("numero")){
                        xtw.writeCharacters(this.columnas.get(i).getPromedio());
                        NewFile += this.columnas.get(i).getPromedio();
                    }
                    else{
                        xtw.writeCharacters(this.columnas.get(i).getModa());
                        NewFile += this.columnas.get(i).getModa();
                    }
                }
                else{
                   xtw.writeCharacters(valorHijo); 
                   NewFile += valorHijo;
                }    
                //System.out.print("|");
                xtw.writeEndElement();
                i++;
                if(i == numCol){
                    NewFile += "\n";
                }else{
                    NewFile += ",";
                }
            } 
            //System.out.println();
            xtw.writeCharacters("\n");
            xtw.writeEndElement();
            
        }
        xtw.writeCharacters("\n");
        xtw.writeEndElement();
        xtw.writeEndDocument();
        
        xtw.flush();
        xtw.close();
        
        
    }
    
    public void NewCSV() throws IOException{

        String ubicacion = ruta.replace(".xml", "cls.csv");
        BufferedWriter bw;
        
        File archivo = new File(ubicacion);
        
        if(archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(NewFile);
        }
        else {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(NewFile);
        }
        bw.close();
        
    }
}
    
   