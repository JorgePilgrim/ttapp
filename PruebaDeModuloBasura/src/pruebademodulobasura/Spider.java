/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebademodulobasura;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorge
 */
public class Spider {
    
    private String currentUrl;
    private  String boolTiposArchivos;
    private String pagesToVisit;
    private String RuteXML;
    private String RuteCSV;
    private List<String> files;
    private List<String> nombres;
    
    public Spider(String pagesToVisit, String boolTiposArchivos){
        this.pagesToVisit = pagesToVisit;
        this.boolTiposArchivos = boolTiposArchivos;
    }
    
    public void extract()
  {
      files = new ArrayList<>();
      currentUrl= pagesToVisit;
      System.out.println("Visitando "+currentUrl);
      String uno = "1";
      
      SpiderLeg leg = new SpiderLeg();
      files = leg.crawl(currentUrl);  
      nombres = leg.JustFiles(files);
      files = leg.JustURL(files);
      
      Object[] options = nombres.toArray();
      Object value = JOptionPane.showInputDialog(null, 
                                           "Seleccione su Archivo", 
                                           "Archivos", 
                                            JOptionPane.QUESTION_MESSAGE, 
                                            null,
                                            options, 
                                            options[0]);
      int index = nombres.indexOf(value);

         
          //CSV 
          if(uno.equals(""+boolTiposArchivos.charAt(0))){
            
            boolean success = leg.searchForCSV(files.get(index),nombres.get(index),currentUrl);
            
            if(success)
            {
                System.out.println("CSV en "+currentUrl+" "+success);
                RuteXML = leg.getXML();
                RuteCSV = leg.getCSV();
            }
            else{
                System.out.println("No hay archivos CSV");
            }
          }
          /* 
          //JSON
          if(uno.equals(""+boolTiposArchivos.charAt(1))){
            boolean success2 = leg.searchForJSON(leg.links,currentUrl);
            if(success2)
            {
                System.out.println(String.format("JSON en "+currentUrl+" "+success2));
            }
            else{
                System.out.println("No hay archivos JSON");
            }
          }
          
          //XML
          if(uno.equals(""+boolTiposArchivos.charAt(2))){
              //System.out.println("entre");
            boolean success3 = leg.searchForXML(leg.links,currentUrl);
            if(success3)
            {
                System.out.println(String.format("XML en "+currentUrl+" "+success3));
            }
            else{
                System.out.println("No hay archivos XML");
            }
          }
         */ 
      }
    public String getXML(){
        return RuteXML;
    }
    
    public String getCSV(){
        return RuteCSV;
    }
    
  }
    

